package com.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.BookDetails;
import com.app.entities.CartItems;
import com.app.entities.UserEntity;

public interface CartRepository extends JpaRepository<CartItems, Long> {

	List<CartItems> findByUser(UserEntity user);
	
	CartItems findByUserAndBook(UserEntity user, BookDetails book);
}
