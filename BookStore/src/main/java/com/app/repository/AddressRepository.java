package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.entities.UserAddress;

public interface AddressRepository extends JpaRepository<UserAddress, Long> {

}
