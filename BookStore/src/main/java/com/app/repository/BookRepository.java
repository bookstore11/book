package com.app.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.BookCategory;
import com.app.entities.BookDetails;
import com.app.entities.UserEntity;

public interface BookRepository extends JpaRepository<BookDetails, Long> {

	@Query("select book from BookDetails book join fetch book.category where book.category.name=?1")
	List<BookDetails> findByCategory(String categoryName);
	
	List<BookDetails> findByName(String name);
	List<BookDetails> findByAuthor(String author);
}
