package com.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.BookCategory;

public interface CategoryRepository extends JpaRepository<BookCategory, Long> {
	Optional<BookCategory> findByName(String name);
}
