package com.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.BookDetails;
import com.app.entities.BookInventory;
import com.app.entities.UserEntity;

public interface BookInventoryRepository extends JpaRepository<BookInventory, Long> {

	
}
