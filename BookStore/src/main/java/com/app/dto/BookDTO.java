package com.app.dto;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import org.springframework.web.multipart.MultipartFile;

import com.app.entities.BookCategory;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookDTO {
	
	@NotBlank(message = "ISBN is required")
	private String isbn;
	
	@NotBlank(message = "Name is required")
	private String name;
	
	@NotBlank(message = "Author name is required")
	private String author;
	
	@NotBlank(message = "Language detail is required")
	private String language;
	
	//@NotBlank(message = "Price info is required")
	private double price;
	
	@NotBlank(message = "Publisher info is required")
	private String publisher;
	
	@NotBlank(message = "Published year is required")
	private String publishYear;
	
	@NotBlank(message = "Format details required")
	private String format;
	
	@NotBlank(message = "Image is required")
	private MultipartFile image;
	
	@JsonProperty(access =Access.READ_ONLY)
	private String imagePath;
	
	@NotBlank(message = "Category is required")
	private BookCategory category;
	
	@NotBlank(message = "Description details required")
	private String description;
	
	private int pages;
}
