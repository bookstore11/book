package com.app.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import com.app.entities.BookDetails;
import com.app.entities.UserEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ViewWishListDTO {
	
	private Set<BookDetails> books = new HashSet<>();
	
	public Set<BookDetails> getBooks() {
		return books;
	}
	
}
