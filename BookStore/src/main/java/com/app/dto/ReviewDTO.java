package com.app.dto;

import javax.validation.constraints.NotBlank;

import com.app.entities.BookDetails;
import com.app.entities.UserEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReviewDTO {
	
	@NotBlank(message = "Title is required")
	private String title;
	
	@NotBlank(message = "Description is required")
	private String description;
	
	private int rating;
	
	@NotBlank(message = "BookId is required")
	private Long book;
	
	@NotBlank(message = "UserId is required")
	private Long user;
}
