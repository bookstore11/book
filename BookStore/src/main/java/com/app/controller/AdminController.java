package com.app.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.BookDTO;
import com.app.dto.BookInventoryDTO;
import com.app.dto.CategoryDTO;
import com.app.dto.DataResponse;
import com.app.dto.ErrorResponse;
import com.app.dto.OrderDetailsDto;

import com.app.entities.DiscountDetails;

import com.app.entities.UserEntity;
import com.app.service.IAdminService;
import com.app.service.IBookInventoryService;
import com.app.service.IBookService;
import com.app.service.ICategoryService;
import com.app.service.IOrderDetailsService;
import com.app.service.ImageHandlingService;

@RestController
@RequestMapping("/admin")
@CrossOrigin(origins = "http://localhost:3000")
public class AdminController {
	@Autowired
	private IAdminService adminService;

	@Autowired
	private IBookInventoryService bookInventoryService;

	@Autowired
	private ICategoryService categoryService;

	@Autowired
	private IBookService bookService;

	@Autowired
	private IOrderDetailsService orderDetailsService;

	@Autowired
	private ImageHandlingService imageService;

	@GetMapping("/viewAll")
	public ResponseEntity<?> showAllBooks() {
		return ResponseEntity.status(HttpStatus.OK).body(new DataResponse("success", bookService.getAllBooks()));
	}

	@DeleteMapping("/deleteBook/{bookId}")
	public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
		return ResponseEntity.status(HttpStatus.OK).body(bookService.deleteBook(bookId));
	}

	@PostMapping("/addBook")
	public ResponseEntity<?> addBookDetails(@ModelAttribute BookDTO book) throws IOException {
		System.out.println("Inside addBook");
		// BookDTO book = new BookDTO(isbn, name, author, language,price, publisher,
		// publishYear, format, imagePath, category);
		book.setImagePath(imageService.uploadImage(book.getImage()));
		return ResponseEntity.status(HttpStatus.CREATED).body(bookService.addBookDetails(book));
	}

	@GetMapping("/view")
	public String showHomePage() {
		return "Admin Home reached";
	}

	@GetMapping("/viewUsers")
	public List<UserEntity> fetchAllUserDetails() {
		System.out.println(" fetch all customers list");
		return adminService.getAllUsers();

	}

	@GetMapping("/viewDiscounts")
	public List<DiscountDetails> fetchAllDiscountDetails() {

		System.out.println(" fetch all discount list");
		return adminService.getAllDiscounts();

	}

	@PostMapping("/addDiscount")

	public DiscountDetails addDiscount(@RequestBody DiscountDetails discount) {
		System.out.println("add new discounts" + discount);// id : null
		return adminService.insertDiscount(discount);
	}

	@PutMapping("/updateDiscount")
	public DiscountDetails updatediscountDetails(@RequestBody DiscountDetails discount) {
		System.out.println(" update  discount details" + discount);// not null id
		return adminService.updatediscountDetails(discount);
	}

	@DeleteMapping("/{uid}")
	public String deleteDiscountDetails(@PathVariable long uid) {

		return adminService.deleteDiscountDetails(uid);
	}

	@DeleteMapping("/deleteUser/{userid}")
	public String deleteUser(@PathVariable long userid) {

		return adminService.deleteUser(userid);
	}

//	@PostMapping("/addBook")
//	public ResponseEntity<?> addBookDetails

	@GetMapping("/inventory")
	public ResponseEntity<?> getAllBooksFromInventory() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bookInventoryService.getAllBooksFromInventory());

		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while fetching inventory details ", e.toString()));
		}
	}

	@PutMapping("/inventory/updateQuantity")
	public ResponseEntity<?> updateQuantity(@RequestBody @Valid BookInventoryDTO bookInv) {

		try {
			// o.s.http.ResponseEntity(T body,HttpStatus stsCode)
			return new ResponseEntity<>(
					bookInventoryService.updateInventoryStock(bookInv.getInventoryId(), bookInv.getQty()),
					HttpStatus.OK);
		} catch (RuntimeException e) {
			System.out.println("err in admin controller " + e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while updating book quantity ", e.toString()));
		}

	}

	@GetMapping("/getBookDetails/{bid}")
	public ResponseEntity<?> getBookDetailsById(@PathVariable long bid) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bookService.getBookDetailsById(bid));

		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong ", e.toString()));
		}

	}

	@GetMapping("/getOrderDetails/{status}")
	public ResponseEntity<?> getOrderDetailsByStatus(@PathVariable String status) {

		try {
			return ResponseEntity.status(HttpStatus.OK).body(orderDetailsService.getOrdersByStatus(status));

		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong. Unable to fetch order details ", e.toString()));
		}
	}

	@PutMapping("/orders/updateStatus")
	public ResponseEntity<?> updateQuantity(@RequestBody OrderDetailsDto orderDto) {

		try {
			// o.s.http.ResponseEntity(T body,HttpStatus stsCode)
			return new ResponseEntity<>(orderDetailsService.updateOrderStatus(orderDto.getId(), orderDto.getStatus()),
					HttpStatus.OK);
		} catch (RuntimeException e) {
			System.out.println("err in admin controller " + e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while updating quantity", e.toString()));
		}

	}

	@PostMapping("/addCategory")
	public ResponseEntity<?> addCategory(@RequestBody CategoryDTO category) {
		try {
			System.out.println(category.getName());
			return ResponseEntity.status(HttpStatus.OK).body(categoryService.addCategory(category));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ErrorResponse("Category addition failed", e.toString()));
		}
	}

	@PutMapping("/editBook/{bookId}")
	public ResponseEntity<?> editBookDetails(@ModelAttribute BookDTO book, @PathVariable long bookId)
			throws IOException {
		try {
			System.out.println("Inside editBook");
			// BookDTO book = new BookDTO(isbn, name, author, language,price, publisher,
			// publishYear, format, imagePath, category);
			book.setImagePath(imageService.uploadImage(book.getImage()));
			return ResponseEntity.status(HttpStatus.OK).body(bookService.editBookDetails(book, bookId));

		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while updating book details ", e.toString()));
		}
	}
}
