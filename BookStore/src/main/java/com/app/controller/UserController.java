package com.app.controller;

import java.io.IOException;

import java.util.Set;

import java.util.List;
import java.util.Set;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ApiResponse;
import com.app.dto.CartDTO;
import com.app.dto.CategoryDTO;
import com.app.dto.DataResponse;
import com.app.dto.ErrorResponse;
import com.app.dto.OrderDetailsDto;
import com.app.dto.UserDTO;
import com.app.entities.DiscountDetails;

import com.app.dto.ReviewDTO;

import com.app.entities.UserAddress;

import com.app.dto.WishListDTO;
import com.app.entities.BookDetails;
import com.app.entities.UserEntity;
import com.app.service.IAdminService;
import com.app.service.IBookInventoryService;
import com.app.service.IBookService;
import com.app.service.ICartService;
import com.app.service.IOrderDetailsService;
import com.app.service.IOrderItemsService;
import com.app.service.IUserService;
import com.app.service.ImageHandlingService;
import com.app.service.IWishlistService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

	@Autowired
	private ICartService cartService;
	// private UserController userService;
	@Autowired
	private IUserService userService;

	@Autowired
	private IOrderDetailsService orderDetailsService;

	@Autowired
	private IBookService bookService;

	@Autowired
	private IWishlistService wishlistService;

	@Autowired
	private IOrderItemsService orderItemsService;

	@PostMapping("/addToCart")
	public ResponseEntity<?> addToCart(@RequestBody CartDTO cart) throws IOException {
		// System.out.println("Inside addToCart : "+cart.getQuantity());
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(cartService.addBookToCart(cart));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while adding book to cart", e.toString()));
		}

	}

	@GetMapping("/viewCart")
	public ResponseEntity<?> viewCart(@RequestBody CartDTO cart) throws IOException {

		try {
			System.out.println("in view cart " + cart.getUserId());
			return ResponseEntity.status(HttpStatus.OK).body(cartService.viewBooksFromCart(cart.getUserId()));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while adding book to cart", e.toString()));
		}
	}

	@DeleteMapping("/deleteBookFromCart/{cartId}")
	public ResponseEntity<?> deleteBookFromCart(@PathVariable Long cartId) throws IOException {

		try {
			System.out.println("in view cart " + cartId);
			return ResponseEntity.status(HttpStatus.OK).body(cartService.deleteBookFromCart(cartId));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while removing book from cart", e.toString()));
		}

	}

	@PutMapping("/updateAccountDetails/{userId}")
	public UserEntity updateaccountDetails(@PathVariable(value = "userId") Long userId,
			@RequestBody UserEntity account) {
		System.out.println(" update  account details" + account);// not null id
		return userService.updateAccountDetails(userId, account);
	}

	@GetMapping("/view")
	public ResponseEntity<?> getBookByCategory(@RequestParam String category) {
//		System.out.println(category.getName());
		return ResponseEntity.status(HttpStatus.OK).body(new DataResponse("success", bookService.getBookByCategory(category)));
	}

	@GetMapping("/search/name")
	public ResponseEntity<?> getBooksByName(@RequestParam String name) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bookService.findBookByName(name));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ErrorResponse("Category addition failed", e.toString()));
		}
	}

	@GetMapping("/search/author")
	public ResponseEntity<?> getBooksByAuthor(@RequestParam String author) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bookService.findBookByAuthor(author));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.OK)
					.body(new ErrorResponse("Category addition failed", e.toString()));
		}
	}

	@PutMapping("/orders/updateStatus")
	public ResponseEntity<?> cancelOrder(@RequestBody OrderDetailsDto orderDto) {

		try {
			// o.s.http.ResponseEntity(T body,HttpStatus stsCode)
			return new ResponseEntity<>(orderDetailsService.updateOrderStatus(orderDto.getId(), "CANCELLED"),
					HttpStatus.OK);
		} catch (RuntimeException e) {
			System.out.println("err in user controller " + e);
			return new ResponseEntity<>(new ApiResponse(e.getMessage()), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/viewOrderHistory/{userId}")
	public ResponseEntity<?> viewCart(@PathVariable long userId) throws IOException {

		try {
			System.out.println("in view all orders " + userId);
			return ResponseEntity.status(HttpStatus.OK).body(orderDetailsService.getOrdersByUser(userId));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while fetching order history", e.toString()));
		}
	}

	@PostMapping("/addToWishlist")
	public ResponseEntity<?> addToWishlist(@RequestBody WishListDTO wishlist) throws IOException {

		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(wishlistService.addBookToWishlist(wishlist));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while adding book to wishlist", e.toString()));
		}

	}

	@GetMapping("/viewWishlist/{userId}")
	public ResponseEntity<?> viewWishlist(@PathVariable long userId) throws IOException {

		try {
			System.out.println("in view all orders " + userId);
			List<BookDetails> userWishlist = wishlistService.getUserWishlist(userId);
			if (userWishlist == null)
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
			return ResponseEntity.status(HttpStatus.OK).body(userWishlist);
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong fetching wishlist ", e.toString()));
		}
	}

	@DeleteMapping("/wishlist/remove")
	public ResponseEntity<?> removeFromWishlist(@RequestBody WishListDTO wishlist) throws IOException {

		try {
			return ResponseEntity.status(HttpStatus.OK)
					.body(wishlistService.removeBookFromWishlist(wishlist.getUserId(), wishlist.getBookId()));

		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong removing book from wishlist ", e.toString()));
		}

	}

	@PutMapping("/cart/incrementBookQuantity/{cartId}")
	public ResponseEntity<?> incrementBookQuantity(@PathVariable long cartId) throws IOException {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(cartService.incrementBookQuantityInCart(cartId));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while increasing book quantity", e.toString()));
		}

	}

	@GetMapping("/getOrderDetails/{orderDetailsId}")
	public ResponseEntity<?> getOrderDetails(@PathVariable long orderDetailsId) throws IOException {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(orderItemsService.getOrderItems(orderDetailsId));

		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while fetching order details", e.toString()));
		}

	}

	@PostMapping("/addNewAddress")
	public UserAddress addNewAddress(@RequestBody UserAddress address) {
		System.out.println("add new address" + address);
		return userService.insertAddress(address);
	}

	@PostMapping("/addBookReview")
	public ResponseEntity<?> addBookReview(@RequestBody ReviewDTO review) {
		try {
			return ResponseEntity.status(HttpStatus.CREATED).body(bookService.addBookReview(review));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while adding review", e.toString()));
		}
	}

	@GetMapping("/getReviews/{bookId}")
	public ResponseEntity<?> getReviews(@PathVariable Long bookId) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(bookService.getReviews(bookId));
		} catch (RuntimeException e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body(new ErrorResponse("Something went wrong while getting reviews", e.toString()));
		}
	}
}
