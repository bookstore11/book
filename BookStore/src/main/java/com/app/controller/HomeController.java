package com.app.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.DataResponse;
import com.app.service.ICategoryService;
import com.app.service.ImageHandlingService;

@RestController
@RequestMapping("/home")
@CrossOrigin(origins = "http://localhost:3000")
public class HomeController {
	
	@Autowired
	private ImageHandlingService imageService;
	
	@Autowired
	private ICategoryService categoryService;

	@GetMapping(value="/{bookId}/images",produces = 
		{MediaType.IMAGE_GIF_VALUE,MediaType.IMAGE_JPEG_VALUE,MediaType.IMAGE_PNG_VALUE})
	public ResponseEntity<?> downloadImage(@PathVariable long bookId) throws IOException {
		return ResponseEntity.ok(imageService.restoreImage(bookId));
	}
	
	@GetMapping("/getCategories")
	public ResponseEntity<?> getCategories(){
		return ResponseEntity.ok(new DataResponse("success", categoryService.getCategories()));
	}
}
