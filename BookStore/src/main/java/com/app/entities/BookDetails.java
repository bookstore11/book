package com.app.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BookDetails extends BaseEntity {
	
	@NotBlank(message = "ISBN is required")
	@Column(length = 16, nullable = false)
	private String isbn;
	
	@NotBlank(message = "Name is required")
	@Column(length = 50, nullable = false)
	private String name;
	
	@NotBlank(message = "Author name is required")
	@Column(length = 50, nullable = false)
	private String author;
	
	@NotBlank(message = "Language detail is required")
	@Column(length = 16, nullable = false)
	private String language;
	
	//@NotBlank(message = "Price info is required")
	//@Column(length = 16, nullable = false)
	private double price;
	
	@NotBlank(message = "Publisher info is required")
	@Column(length = 50, nullable = false)
	private String publisher;
	
	@NotBlank(message = "Published year is required")
	@Column(length = 16, nullable = false)
	private String publishYear;
	
	@NotBlank(message = "Format details required")
	@Column(length = 16, nullable = false)
	private String format;
	
	@NotBlank(message = "Image path is required")
	@Column(length = 100, nullable = false)
	private String imagePath;
	
	@ManyToOne
	@JoinColumn(nullable = false)
	private BookCategory category;
	
	@NotBlank(message = "Description details required")
	@Column(length = 500, nullable = false)
	private String description;
	
	private int pages;
}
