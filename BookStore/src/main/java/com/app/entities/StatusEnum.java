package com.app.entities;

public enum StatusEnum {

	ORDERED, INTRANSIT, CANCELLED, DELIVERED
}
