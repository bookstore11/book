package com.app.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString

public class Wishlist extends BaseEntity {
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = false)
	private UserEntity user; 
	

	// Wishlist 1----->* Books
	@ElementCollection 
	@CollectionTable(name = "wishlist_books", joinColumns = @JoinColumn(name = "wishlist_id")) 																			// the coll table + FK col name
	private Set<BookDetails> books = new HashSet<>();

	public Wishlist()
	{
		
	}

	public void addBook(BookDetails book) {
		books.add(book);
	}

	public void removeBook(BookDetails book) {
		books.remove(book);
	}
	
	public Set<BookDetails> getBooks() {
		return books;
	}

	public void setBooks(Set<BookDetails> books) {
		this.books = books;
	}


}
