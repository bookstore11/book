package com.app.service;

import java.util.List;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;

import com.app.entities.BookInventory;

public interface IBookInventoryService {
	
	List<BookInventory> getAllBooksFromInventory();
	
	BookInventory updateInventoryStock(Long id, double qty);
}
