package com.app.service;

import java.util.List;

import com.app.dto.LoginResponse;
import com.app.dto.OrderItemsDTO;
import com.app.dto.UserDTO;

import com.app.entities.BookDetails;
import com.app.entities.BookInventory;
import com.app.entities.OrderDetails;
import com.app.entities.OrderItems;
import com.app.entities.StatusEnum;

public interface IOrderItemsService {
	
	
	
	List<OrderItemsDTO> getOrderItems(long orderDetailsId);

}
