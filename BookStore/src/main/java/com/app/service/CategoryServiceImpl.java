package com.app.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.ApiResponse;
import com.app.dto.CategoryDTO;
import com.app.entities.BookCategory;
import com.app.repository.CategoryRepository;

@Service
@Transactional
public class CategoryServiceImpl implements ICategoryService {

	@Autowired
	private CategoryRepository catRepo;
	
	@Autowired
	private ModelMapper mapper;
	
	@Override
	public ApiResponse addCategory(CategoryDTO category) {
		BookCategory categoryDetails = mapper.map(category, BookCategory.class);
		System.out.println(category.getName());
		System.out.println(categoryDetails.getName());
		catRepo.save(categoryDetails);
		return new ApiResponse("Caregory added successfully!");
		
	}

	@Override
	public List<BookCategory> getCategories() {
		List<BookCategory> categories = catRepo.findAll();
		return categories;
	}
	
}
