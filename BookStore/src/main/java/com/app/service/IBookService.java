package com.app.service;

import com.app.entities.BookDetails;
import com.app.entities.ReviewDetails;

import java.util.List;

import com.app.dto.ApiResponse;
import com.app.dto.BookDTO;
import com.app.dto.CategoryDTO;
import com.app.dto.CreatedResponse;
import com.app.dto.ReviewDTO;

public interface IBookService {
	CreatedResponse addBookDetails(BookDTO book);
	List<BookDetails> getAllBooks();
	ApiResponse deleteBook(Long bookId);

	BookDetails getBookDetailsById(Long id);
	
	List<BookDetails> getBookByCategory(String category);
	
	List<BookDetails> findBookByName(String name);
	
	List<BookDetails> findBookByAuthor(String author);
	
	CreatedResponse editBookDetails(BookDTO newBook, long bid);

	ApiResponse addBookReview(ReviewDTO reviewDetails);
	
	List<ReviewDetails> getReviews(Long bookId);

}
