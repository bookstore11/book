package com.app.service;


import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.CreatedResponse;
import com.app.dto.WishListDTO;
import com.app.entities.BookDetails;
import com.app.entities.UserEntity;
import com.app.entities.Wishlist;
import com.app.repository.BookRepository;
import com.app.repository.UserRepository;
import com.app.repository.WishlistRepository;

@Service
@Transactional
public class WishlistServiceImpl implements IWishlistService {

	@Autowired
	private WishlistRepository wishlistRepo;;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private BookRepository bookRepo;

	@Override
	public CreatedResponse addBookToWishlist(WishListDTO wishlist) {
		// TODO Auto-generated method stub
		BookDetails book = bookRepo.findById(wishlist.getBookId()).orElseThrow(() -> new RuntimeException("Book Not Found!"));
		UserEntity user = userRepo.findById(wishlist.getUserId()).orElseThrow(() -> new RuntimeException("User Not Found!"));
		Wishlist existingWishList = wishlistRepo.findByUser(user);
		
		if(existingWishList != null)
		{		
			System.out.println("In IF");
			existingWishList.addBook(book);
			wishlistRepo.save(existingWishList);
		}
		else {
			System.out.println("In ELSE");
			Wishlist wishList = new Wishlist();
			wishList.setUser(user);
			wishList.addBook(book);
			wishlistRepo.save(wishList);
		}
		
		return new CreatedResponse("Book Added to wishlist successfully " );
	}


	@Override
	public List<BookDetails> getUserWishlist(long userId) {
		// TODO Auto-generated method stub
		UserEntity user = userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User Not Found!"));

		System.out.println("USER" + user);
		System.out.println("WISHLIST" + wishlistRepo.findByUser(user));
		if(wishlistRepo.findByUser(user)==null)
			return null;
		List<BookDetails> books = wishlistRepo.findByUser(user).getBooks().stream().collect(Collectors.toList());
		return books;
	}


	@Override
	public CreatedResponse removeBookFromWishlist(long userId, long bookId) {
		// TODO Auto-generated method stub
		BookDetails book = bookRepo.findById(bookId).orElseThrow(() -> new RuntimeException("Book Not Found!"));
		UserEntity user = userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User Not Found!"));
		Wishlist existingWishList = wishlistRepo.findByUser(user);
		//System.out.println("WISHLIST" + existingWishList);
		existingWishList.removeBook(book);
		wishlistRepo.save(existingWishList);
		return new CreatedResponse("Book removed from wishlist successfully " );

	}

	
}
