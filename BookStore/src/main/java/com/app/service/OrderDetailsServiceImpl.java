package com.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;

import com.app.entities.BookDetails;
import com.app.entities.BookInventory;
import com.app.entities.OrderDetails;
import com.app.entities.StatusEnum;
import com.app.entities.UserEntity;
import com.app.repository.BookRepository;
import com.app.repository.OrderDetailsRepository;
import com.app.repository.RoleRepository;
import com.app.repository.UserRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class OrderDetailsServiceImpl implements IOrderDetailsService {

	@Autowired
	private OrderDetailsRepository orderDetailsRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public List<OrderDetails> getOrdersByStatus(String status) {
		// TODO Auto-generated method stub
		System.out.println("SERVICE : "+status);
		//List<OrderDetails> orderList = 
		return orderDetailsRepo.findByStatus(status);
	}

	@Override
	public OrderDetails updateOrderStatus(long id, String status) {
		// TODO Auto-generated method stub
		
		OrderDetails od = orderDetailsRepo.findById(id).orElseThrow();
		
		od.setStatus(status);
		od.setStatusUpdatedAt(Date.valueOf(LocalDate.now()));
		return od;
	}

	@Override
	public List<OrderDetails> getOrdersByUser(long userId) {
		// TODO Auto-generated method stub
		UserEntity user = userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
		System.out.println("IN service impl "+ user);
		return orderDetailsRepo.findByUser(user);
	}

	
	
}
