package com.app.service;

import java.util.List;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;

import com.app.entities.BookDetails;
import com.app.entities.BookInventory;
import com.app.entities.OrderDetails;
import com.app.entities.StatusEnum;

public interface IOrderDetailsService {
	
	//get all orders with ordered status
	
	List<OrderDetails> getOrdersByStatus(String status);
	
	OrderDetails updateOrderStatus(long id, String status);
	
	List<OrderDetails> getOrdersByUser(long userId);
	
}
