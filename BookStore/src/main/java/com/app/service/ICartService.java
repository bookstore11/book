package com.app.service;

import java.util.List;

import com.app.dto.CartDTO;
import com.app.dto.CreatedResponse;
import com.app.entities.CartItems;

public interface ICartService {
	CreatedResponse addBookToCart(CartDTO cart);

	List<CartItems> viewBooksFromCart(long userId);

	CreatedResponse incrementBookQuantityInCart(long cartId) ;
	
	CreatedResponse deleteBookFromCart(long cartId) ;
}
