package com.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;

import com.app.dto.ApiResponse;
import com.app.dto.BookDTO;
import com.app.dto.CartDTO;
import com.app.dto.CreatedResponse;
import com.app.entities.BookCategory;
import com.app.entities.BookDetails;

import com.app.entities.BookDetails;
import com.app.entities.BookInventory;
import com.app.entities.CartItems;
import com.app.entities.UserEntity;
import com.app.repository.BookRepository;
import com.app.repository.CartRepository;
import com.app.repository.CategoryRepository;
import com.app.repository.RoleRepository;
import com.app.repository.UserRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class CartServiceImpl implements ICartService {

	@Autowired
	private CartRepository cartRepo;;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private BookRepository bookRepo;
	

	@Override
	public CreatedResponse addBookToCart(CartDTO cart) {
		// TODO Auto-generated method stub
		CartItems transientCartItem = new CartItems();
		UserEntity user = userRepo.findById(cart.getUserId()).orElseThrow(() -> new RuntimeException("User Not Found!"));
		BookDetails book = bookRepo.findById(cart.getBookId()).orElseThrow(() -> new RuntimeException("Book Not Found!"));
		transientCartItem.setUser(user);
		transientCartItem.setBook(book);
		
		int qty = (cart.getQuantity()==0 ? 1 : cart.getQuantity());
		transientCartItem.setQuantity(qty);
		
		CartItems persistentCartItem = cartRepo.save(transientCartItem);
		return new CreatedResponse("Book Added to cart successfully " );
	}


	@Override
	public List<CartItems> viewBooksFromCart(long userId) {
		// TODO Auto-generated method stub
		UserEntity user = userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
		return cartRepo.findByUser(user);
	}


	@Override
	public CreatedResponse deleteBookFromCart(long cartId) {
		// TODO Auto-generated method stub
		CartItems cartItem = cartRepo.findById(cartId).orElseThrow(() -> new RuntimeException("Cart item not found"));
		cartRepo.delete(cartItem);
		return new CreatedResponse("Book removed from cart successfully " );
	}


	@Override
	public CreatedResponse incrementBookQuantityInCart(long cartId) {
		// TODO Auto-generated method stub
		CartItems cartItem = cartRepo.findById(cartId).orElseThrow(() -> new RuntimeException("Cart item not found"));
		cartItem.setQuantity(cartItem.getQuantity()+1);
		cartRepo.save(cartItem);
		return new CreatedResponse("Book quantity updated. New quantity "+ cartItem.getQuantity());
	}

	
}
