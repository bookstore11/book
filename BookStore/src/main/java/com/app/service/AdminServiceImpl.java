package com.app.service;

import java.util.List;

import javax.persistence.criteria.Join;

import org.hibernate.annotations.Where;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.entities.DiscountDetails;
import com.app.entities.RoleEntity;
import com.app.entities.UserEntity;
import com.app.repository.DiscountRepository;
import com.app.repository.UserRepository;

@Service
@Transactional
public class AdminServiceImpl implements IAdminService {

	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private DiscountRepository discountRepo;
	
	@Override
	public List<UserEntity> getAllUsers() {
		// TODO Auto-generated method stub
		System.out.println("in service class");
		return userRepo.findUserRoles();
	}
	
	@Override
	public List<DiscountDetails> getAllDiscounts() {
		// TODO Auto-generated method stub
		
		System.out.println("in service class");
		return discountRepo.findAll();
	}
	
	@Override
	public DiscountDetails insertDiscount(DiscountDetails discountdetails) {
		// TODO Auto-generated method stub
		
		System.out.println("in service class");
		return discountRepo.save(discountdetails);
	}
	@Override
	public DiscountDetails updatediscountDetails(DiscountDetails discountdetails) {
		discountRepo.findById(discountdetails.getId());
		
		return discountRepo.save(discountdetails);
	}
	@Override
	public String deleteDiscountDetails(long userId) {
		String mesg = "Deleting discount failed !!!!!";
		
		if (discountRepo.existsById(userId)) {
			discountRepo.deleteById(userId);
			mesg = "Deleted discount details of user id " + userId;
		}
		return mesg;

	}
	@Override
	public String deleteUser(long userId) {
		String mesg = "Deleting users  failed !!!!!";
		
		if (userRepo.existsById(userId)) {
			userRepo.deleteById(userId);
			mesg = "Deleted user of user id " + userId;
		}
		return mesg;

	}
}
