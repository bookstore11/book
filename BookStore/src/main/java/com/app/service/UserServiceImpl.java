package com.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;
import com.app.dto.AuthUserDTO;
import com.app.dto.CreatedResponse;
import com.app.entities.DiscountDetails;
import com.app.entities.UserAddress;
import com.app.entities.UserEntity;
import com.app.repository.AddressRepository;
import com.app.repository.RoleRepository;
import com.app.repository.UserRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo;

	@Autowired
	private ModelMapper mapper;
	// password enc
	@Autowired
	private PasswordEncoder encoder;
	
	@Autowired
	private AddressRepository addressRepo;
	
//	@Override
//	public LoginResponse authenticateUser(String email, String password) {
//		UserEntity user = userRepo.findByEmailAndPassword(email, password)
//				.orElseThrow(() -> new RuntimeException("Auth Failed!"));
//		
//		return new LoginResponse(user);
//	}


	@Override
	public CreatedResponse registerUser(UserDTO user) {
		UserEntity userEntity = mapper.map(user, UserEntity.class);
		// 2. Map Set<UserRole : enum> ---> Set<Role :entity> n assign it to the
		// transient user entity
		userEntity.setUserRoles(roleRepo.findByRoleNameIn(user.getRoles()));
		// 3. encode pwd
		userEntity.setPassword(encoder.encode(user.getPassword()));
		userEntity.setJoinedOn(Timestamp.valueOf(LocalDateTime.now()));
		// 4 : Save user details
		UserEntity persistentUser = userRepo.save(userEntity);
		return new CreatedResponse("User registered successfully with ID " + persistentUser.getId());
	}
	
	
	@Override
	public UserEntity updateAccountDetails(Long userId, UserEntity accountdetails) {
		UserEntity user = userRepo.findById(userId)
				 .orElseThrow(() -> 
				 new RuntimeException("User not found for this id :: " + userId));
		user.setFirstName(accountdetails.getFirstName());
		user.setLastName(accountdetails.getLastName());
	    user.setPhone(accountdetails.getPhone());
		return userRepo.save(user);
	}
	
	@Override
	public UserAddress insertAddress(UserAddress address) {
		// TODO Auto-generated method stub
		
		System.out.println("in service class");
		return addressRepo.save(address);
	}


	@Override
	public AuthUserDTO getUserDetails(String email) {
		UserEntity user = userRepo.findByEmail(email).orElseThrow(() -> new RuntimeException("User not found"));
		System.out.println(user);
		AuthUserDTO authUser = mapper.map(user, AuthUserDTO.class);

		authUser.setRoles(user.getUserRoles());
		System.out.println(authUser);
		return authUser;
	}
	

}
