package com.app.service;

import java.util.List;
import java.util.Set;

import com.app.dto.CartDTO;
import com.app.dto.CreatedResponse;
import com.app.dto.WishListDTO;
import com.app.entities.BookDetails;
import com.app.entities.CartItems;
import com.app.entities.Wishlist;

public interface IWishlistService {
	CreatedResponse addBookToWishlist(WishListDTO wishlist);

	List<BookDetails> getUserWishlist(long userId);
	
	CreatedResponse removeBookFromWishlist(long userId, long bookId);


}
