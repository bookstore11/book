package com.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;
import com.app.entities.BookInventory;
import com.app.entities.UserEntity;
import com.app.repository.BookInventoryRepository;
import com.app.repository.BookRepository;
import com.app.repository.RoleRepository;
import com.app.repository.UserRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class BookInventoryServiceImpl implements IBookInventoryService{

	@Autowired
	private BookInventoryRepository bookInventoryRepo;
	
	@Autowired
	private ModelMapper mapper;

	@Override
	public List<BookInventory> getAllBooksFromInventory() {
		// TODO Auto-generated method stub
		return bookInventoryRepo.findAll();
	}

	@Override
	public BookInventory updateInventoryStock(Long id, double qty) {
		// TODO Auto-generated method stub
		//System.out.println("IN SERVICE");
		BookInventory b = bookInventoryRepo.findById(id).orElseThrow();
		b.setQuantity(qty);
		return bookInventoryRepo.save(b);
	}
	
}
