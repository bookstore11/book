package com.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dto.LoginResponse;
import com.app.dto.ReviewDTO;
import com.app.dto.UserDTO;

import com.app.dto.ApiResponse;
import com.app.dto.BookDTO;
import com.app.dto.CategoryDTO;
import com.app.dto.CreatedResponse;
import com.app.entities.BookCategory;
import com.app.entities.BookDetails;

import com.app.entities.BookDetails;
import com.app.entities.BookInventory;
import com.app.entities.ReviewDetails;
import com.app.entities.UserEntity;
import com.app.repository.BookRepository;
import com.app.repository.CategoryRepository;
import com.app.repository.ReviewRepository;
import com.app.repository.RoleRepository;
import com.app.repository.UserRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;

@Service
@Transactional
public class BookServiceImpl implements IBookService {

	@Autowired
	private BookRepository bookRepo;
	
	@Autowired
	private ReviewRepository reviewRepo;

	@Autowired
	private CategoryRepository catRepo;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private UserRepository userRepo;


	@Override
	public CreatedResponse addBookDetails(BookDTO book) {
		BookDetails bookDetails = mapper.map(book, BookDetails.class);
		BookCategory category = catRepo.findByName(book.getCategory().getName()).orElseThrow(() -> new RuntimeException("Category Not Found!"));
		bookDetails.setCategory(category);
		BookDetails persistentBook = bookRepo.save(bookDetails);
		
		return new CreatedResponse("Book Added Successfully with image path " + persistentBook.getImagePath());
	}


	@Override
	public List<BookDetails> getAllBooks() {
		List<BookDetails> books = bookRepo.findAll();
		return books;
	}


	@Override
	public ApiResponse deleteBook(Long bookId) {
		BookDetails book = bookRepo.findById(bookId).orElseThrow(() -> new RuntimeException("Book Not Found!!"));
		bookRepo.delete(book);
		return new ApiResponse("Book Deleted Successfully!");
	}
	

	@Override
	public BookDetails getBookDetailsById(Long id) {
		// TODO Auto-generated method stub
		return bookRepo.findById(id).orElseThrow();
	}


	@Override
	public List<BookDetails> getBookByCategory(String category) {
		return bookRepo.findByCategory(category);
	}


	@Override
	public List<BookDetails> findBookByName(String name) {
		return bookRepo.findByName(name);
	}


	@Override
	public List<BookDetails> findBookByAuthor(String author) {
		return bookRepo.findByAuthor(author);
	}


	@Override
	public CreatedResponse editBookDetails(BookDTO newBook, long bid) {
		// TODO Auto-generated method stub
		BookDetails book = bookRepo.findById(bid).orElseThrow(() -> new RuntimeException("Book Not Found!"));;
		BookCategory category = catRepo.findByName(newBook.getCategory().getName()).orElseThrow(() -> new RuntimeException("Category Not Found!"));
		
		
		book.setAuthor(newBook.getAuthor());
		book.setCategory(category);
		book.setFormat(newBook.getFormat());
		book.setImagePath(newBook.getImagePath());
		book.setIsbn(newBook.getIsbn());
		book.setLanguage(newBook.getLanguage());
		book.setName(newBook.getName());
		book.setPrice(newBook.getPrice());
		book.setPublisher(newBook.getPublisher());
		book.setPublishYear(newBook.getPublishYear());
		book.setDescription(newBook.getDescription());
		book.setPages(newBook.getPages());
		
		bookRepo.save(book);
		
		return new CreatedResponse("Book details edited successfully with image path " + book.getImagePath());
	}
	
	public ApiResponse addBookReview(ReviewDTO reviewDetails) {
		
		UserEntity user = userRepo.findById(reviewDetails.getUser()).orElseThrow(() -> new RuntimeException("User not found!"));
		BookDetails book = bookRepo.findById(reviewDetails.getBook()).orElseThrow(() -> new RuntimeException("Book not found!"));
		ReviewDetails review = mapper.map(reviewDetails, ReviewDetails.class);
		review.setUser(user);
		review.setBook(book);
		reviewRepo.save(review);
		return new ApiResponse("Review Added Successfully!");
	}


	@Override
	public List<ReviewDetails> getReviews(Long bookId) {
		List <ReviewDetails> reviews = reviewRepo.findByBookId(bookId);
		return reviews;

	}

	
}
