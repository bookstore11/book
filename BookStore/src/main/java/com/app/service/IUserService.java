package com.app.service;

import java.util.List;

import com.app.dto.LoginResponse;
import com.app.dto.UserDTO;
import com.app.entities.DiscountDetails;
import com.app.entities.UserAddress;
import com.app.entities.UserEntity;
import com.app.dto.AuthUserDTO;
import com.app.dto.CreatedResponse;

public interface IUserService {
//	LoginResponse authenticateUser(String email, String password);
	
	CreatedResponse registerUser(UserDTO user);

	public UserEntity updateAccountDetails(Long userId, UserEntity accountdetails);
	public UserAddress insertAddress(UserAddress address);
	public AuthUserDTO getUserDetails(String email);

}
