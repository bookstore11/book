import axios from 'axios';
import React, { useState } from 'react'
import { Form,  Row, Col, Container, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import NavbarComp from '../components/NavbarComp'
import config from '../config';

const AddBook = () => {

    const navigate = useNavigate()

    const [name, setName] = useState('')
    const [isbn, setIsbn] = useState('')
    const [publishYear, setPublishYear] = useState('')
    const [price, setPrice] = useState('')
    const [asin, setAsin] = useState('')
    const [format, setFormat] = useState()
    const [category, setCategory] = useState('')
    const [publisher, setPublisher] = useState('')
    const [language, setLanguage] = useState('')
    const [author, setAuthor] = useState('')
    const [pages, setPages] = useState('')
    const [image, setImage] = useState()
    const [overview, setOverview] = useState('')
    const [files, setFiles] = useState([])

    const addBook = () => {
        // const { name, isbn, publishYear, price, asin, format, category, publisher, language, author, pages, image}
        if(name.length === 0){
            toast.error('Please enter Book name!')
        }else if(isbn.length === 0){
            toast.error('Please enter ISBN!')
        }else if(publishYear.length === 0){
            toast.error('Please enter publish year!')
        }else if(price.length === 0){
            toast.error('Please enter price!')
        }else if(asin.length === 0){
            toast.error('Please enter ASIN!')
        }else if(format.length === 0){
            toast.error('Please enter format of book!')
        }else if(category.length === 0){
            toast.error('Please enter category!')
        }else if(publisher.length === 0){
            toast.error('Please enter publisher details!')
        }else if(language.length === 0){
            toast.error('Please enter language!')
        }else if(author.length === 0){
            toast.error('Please enter name of the author!')
        }else if(pages.length === 0){
            toast.error('Please enter number of pages!')
        }else if(files.length === 0){
            toast.error('Please upload image of book cover!')
        }else if(overview.length === 0){
            toast.error('Please add overview of the book!')
        }else{

            const formData = new FormData()

            formData.append('name', name)
            formData.append('isbn', isbn)
            formData.append('publishYear', publishYear)
            formData.append('price', price)
            formData.append('asin', asin)
            formData.append('format', format)
            formData.append('category', category)
            formData.append('publisher', publisher)
            formData.append('language', language)
            formData.append('author', author)
            formData.append('pages', pages)
            formData.append('cover', image)
            formData.append('overview', overview)

            // for (var pair of formData.entries()) {
            //     console.log(pair[0]+ ', ' + pair[1]); 
            // }
            
            axios.post(config.serverURL + '/book/add-book', 
            formData,
            {
                headers: {
                  'Content-Type': 'multipart/form-data',
                  'token': sessionStorage.token
                }
            }).then(response => {
                const result = response.data

                if(result.status === 'success'){
                    toast.success('Book added successfully :)')
                    navigate('/home')

                }else{
                    toast.error('Something went wrong :( Please try again.')
                    console.log(result.error)
                }
            }).catch((error)=>{
                console.log('error')
                console.log(error)
            })
        }
    }

  return (
      <>
    <NavbarComp />
    <Container>
        
        <h3 style={{marginTop:'20px'}}>Add New Book</h3>
        <hr></hr>
        <Row>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Book Name</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setName(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>ISBN</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setIsbn(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Publish Year</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setPublishYear(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
        </Row>
        <Row>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setPrice(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>ASIN</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setAsin(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicGender">
                            <Form.Label>Book Format</Form.Label>
                            <Form.Select onChange={(event) => {
                              setFormat(event.target.value)
                            }} aria-label="Default select example">
                              <option></option>
                              <option value="paperback">Paperback</option>
                              <option value="hard cover">Hard Cover</option>
                            </Form.Select>
                        </Form.Group>
            </Col>
        </Row>
        <Row>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Category</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setCategory(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Publisher</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setPublisher(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Language</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setLanguage(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
        </Row>
        <Row>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Author</Form.Label>
                            <Form.Control type="text" onChange={(event) => {
                                setAuthor(event.target.value)
                            }}  />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Number of Pages</Form.Label>
                            <Form.Control type="number" onChange={(event) => {
                                setPages(event.target.value)
                            }} />
                </Form.Group>
            </Col>
            <Col md={4}>
                <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>Upload a photo of book cover</Form.Label>
                            <Form.Control type="file" onChange={(event) => {
                                setImage(event.target.files[0])
                                setFiles(event.target.files)
                            }}/>
                </Form.Group>
            </Col>
        </Row>
        <Row>
            <Col>
                <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Book Overview</Form.Label>
                    <Form.Control onChange={(event) => {
                                setOverview(event.target.value)
                            }} style={{resize:'none'}} as="textarea" maxLength={1000} rows={5} />
                </Form.Group>
            </Col>
        </Row>
        <Row>
            <Col>
                <Button onClick={addBook} className="mb-3" style={{color:'white'}} variant="info">
                  Add Book
                </Button>
            </Col>
            <Col></Col>
            <Col></Col>
            
        </Row>
    </Container>
      </>
    
  )
}

export default AddBook