import React from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import NavbarComp from "../components/NavbarComp";
import { FcCheckmark } from "react-icons/fc";
import { MdDeleteForever } from "react-icons/md";

const ShoppingCart = () => {
  const styles = {
    h2: {
      marginTop: "5%",
    },
    img: {
      width: "auto",
      height: "200px",
      borderRadius: "15px",
    },
    col: { marginTop: "10px", marginBottom: "10px" },
  };

  return (
    <div>
      <NavbarComp />
      <Container>
        <Row>
          <Col sm={12} md={8}>
            <h2 style={styles.h2}>Shopping Cart</h2>
            <hr />
            {/* Repeating component start here below */}
            <Row>
              <Col style={styles.col} sm={12}>
                <Card style={{ borderRadius: "15px" }}>
                  <Card.Body>
                    <Row>
                      <Col style={{ textAlign: "center" }} md={3}>
                        <img
                          src="https://m.media-amazon.com/images/I/51Z0nLAfLmL.jpg"
                          style={styles.img}
                          alt="book-cover"
                        />
                      </Col>
                      <Col md={9}>
                        <Card.Title>
                          <h4>The Alchemist - by Paulo Coelho</h4>
                        </Card.Title>
                        <Card.Text>
                          <h5>
                            <b>₹201 </b>(Inclusive of all taxes)
                          </h5>
                          <h5 style={{ color: "green" }}>In stock.</h5>
                        </Card.Text>
                        <h5 style={{ display: "inline" }}>Quantity : </h5>
                        <Form.Select
                          style={{
                            marginBottom: "10px",
                            width: "70px",
                            display: "inline-block",
                          }}
                          aria-label="Default select example">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </Form.Select>
                        <Button
                          className="mb-1"
                          style={{
                            width: "200px",
                            color: "white",
                            backgroundColor: "red",
                            border: "none",
                            display: "block",
                          }}
                          variant="info">
                          <MdDeleteForever style={{ fontSize: "18px" }} />{" "}
                          Remove
                        </Button>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col sm={12} md={4}>
            <div style={{ marginTop: "10%" }}>
              <Card style={{ width: "18rem", borderRadius: "15px" }}>
                <Card.Body>
                  <Card.Title>
                    <h4>
                      <b>Buy Now : </b>₹201 (Inclusive of all taxes)
                    </h4>
                  </Card.Title>
                  <Card.Text>
                    <br />
                    <br />
                    <h5 style={{ color: "green" }}>
                      <FcCheckmark /> Free Delivery.
                    </h5>
                  </Card.Text>
                  <Button
                    className="mb-1"
                    style={{ width: "100%", color: "white" }}
                    variant="info">
                    Proceed To Checkout
                  </Button>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ShoppingCart;
