import React, { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import config from "../config";
import { Link, useNavigate } from "react-router-dom";
import { Form, Row, Col, Container, Button } from "react-bootstrap";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const signIn = () => {
    if (email.length === 0) {
      toast.error("Please enter email!");
    } else if (password.length === 0) {
      toast.error("Please enter password!");
    } else {
      axios
        .post(config.serverURL + "/auth/signin", {
          email,
          password,
        })
        .then((response) => {
          const result = response.data;

          if (result.message !== "Auth successful!") {
            console.log(result);
            toast.error("Invalid email or password!");
          } else {
            sessionStorage["jwt"] = result.jwt;
            sessionStorage["username"] =
              result.data.firstName + result.data.lastName;
            sessionStorage["email"] = result.data.email;
            sessionStorage["phone"] = result.data.phone;
            sessionStorage["gender"] = result.data.gender;
            sessionStorage["role"] = result.data.roles[0].roleName;
            toast.success(
              "Welcome " +
                sessionStorage.user_details.lastName +
                sessionStorage.user_details.firstName
            );
            if (sessionStorage["role"] === "USER") navigate("/home");
            else navigate("/admin-home");
          }
        })
        .catch((error) => {
          console.log("error");
          console.log(error);
        });
    }
  };

  return (
    <>
      <Container>
        <Row className="mt-5">
          <Col
            style={{ backgroundColor: "white", borderRadius: "10px" }}
            className="p-5 m-auto rounded-lg"
            lg={5}
            md={6}
            sm={12}>
            <h1 className="text-secondary mt-3 mb-5 text-center rounded">
              Welcome to Bookworm
            </h1>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  onChange={(event) => {
                    setEmail(event.target.value);
                  }}
                  type="email"
                  placeholder="Enter email"
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  onChange={(event) => {
                    setPassword(event.target.value);
                  }}
                  type="password"
                  placeholder="Password"
                />
              </Form.Group>
              <div className="d-grid">
                <Button
                  onClick={signIn}
                  style={{ color: "white" }}
                  variant="info">
                  Login
                </Button>
              </div>
              <Form.Text as={Link} to="/register" className="text-secondary">
                Not a member? Register here
              </Form.Text>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Login;
