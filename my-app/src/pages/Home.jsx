import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import NavbarComp from "../components/NavbarComp";
import SearchBar from "../components/SearchBar";
import BookSection from "../components/BookSection";
import axios from "axios";
import config from "../config";
import { toast } from "react-toastify";

const Home = () => {
  useEffect(() => {
    loadBooks("Fiction");
  }, []);
  const [bookDetails, setBookDetails] = useState([]);

  const loadBooks = (category) => {
    axios
      .get(config.serverURL + "/user/view?category=" + category, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage["jwt"]}`,
        },
      })
      .then((response) => {
        const result = response.data;
        if (result.message === "success") {
          console.log(result.data);
          setBookDetails(result.data);
        } else {
          toast.error(result.error);
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  return (
    <>
      <NavbarComp />
      <div>
        <Container>
          <SearchBar />
          <BookSection title="Fiction" bookDetails={bookDetails} />
          {/* <BookCard /> */}
        </Container>
      </div>
    </>
  );
};

export default Home;
