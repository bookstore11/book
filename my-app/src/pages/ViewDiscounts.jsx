import React, { useState } from "react";
import { Form, Container, Button, Table } from "react-bootstrap";
import { MdDeleteForever } from "react-icons/md";
import NavbarComp from "../components/NavbarComp";


const ViewDiscounts = () => {
  const discountList = [
    {
      id: 1,
      disc: {
        id: 1,

        couponCode: "FlashSale40",
        validDate: "2022-12-12",
        percentage: "40",
        createdAt: "2022-09-09",
      },
    },

    {
      id: 2,
      disc: {
        id: 2,

        couponCode: "FlashSale50",
        validDate: "2022-12-10",
        percentage: "50",
        createdAt: "2022-09-10",
      },
    },
    {
      id: 3,
      disc: {
        id: 3,

        couponCode: "FlashSale55",
        validDate: "2022-12-12",
        percentage: "55",
        createdAt: "2022-09-08",
      },
    },
    {
      id: 4,
      disc: {
        id: 4,

        couponCode: "FlashSale45",
        validDate: "2022-12-12",
        percentage: "45",
        createdAt: "2022-05-07",
      },
    },

    {
      id: 5,
      disc: {
        id: 5,

        couponCode: "FlashSale10",
        validDate: "2022-02-12",
        percentage: "10",
        createdAt: "2022-09-22",
      },
    },

    {
      id: 6,
      disc: {
        id: 6,

        couponCode: "FlashSale90",
        validDate: "2022-05-02",
        percentage: "90",
        createdAt: "2021-11-05",
      },
    },
    {
      id: 7,
      disc: {
        id: 7,

        couponCode: "FlashSale25",
        validDate: "2022-12-12",
        percentage: "25",
        createdAt: "2022-07-02",
      },
    },
  ];


  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: "20px" }}>Manage Discounts</h3>
        <hr></hr>
        <Table striped>
          <thead>
            <tr>
              <th>#ID</th>
              <th>Coupon Code</th>
              <th>Valid Date</th>
              <th>Percentage</th>
              <th>Created At</th>
            </tr>
          </thead>
          <tbody>
            {discountList.map((discount) => {
              return (
                <tr>
                  <td>{discount.id}</td>
                  <td>{discount.disc.couponCode}</td>
                  <td>{discount.disc.validDate}</td>
                  <td>{discount.disc.percentage}</td>
                  <td>{discount.disc.createdAt}</td>

                  <td>
                    <Button
                      onClick={() => {}} //onclick
                      style={{ color: "white" }}
                      variant="info"
                    >
                      Update
                    </Button>
                  </td>
                  <td>
                  <Button
                    className="mb-3"
                    style={{
                      //width: "50%",
                      color: "white",
                      backgroundColor: "red",
                      border: "none",
                      marginLeft: "20px",
                    }}
                    variant="info"
                  >
                    <MdDeleteForever style={{ fontSize: "18px" }} /> Delete
                  </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Container>
    </>
  );
};

export default ViewDiscounts;
