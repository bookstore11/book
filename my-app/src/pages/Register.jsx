import React, { useState } from 'react'
import { Link, useNavigate } from "react-router-dom"
import { Form,  Row, Col, Container, Button } from 'react-bootstrap';
import { toast } from 'react-toastify';
import axios from 'axios';
import config from '../config';

const Register = () => {

  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [phone, setPhone] = useState('')
  const [gender, setGender] = useState('')

  const navigate = useNavigate()

  const signup = () => {
    if(firstName.length === 0){
      toast.error("Please enter first name!")
    }else if(lastName.length === 0){
      toast.error("Please enter last name!")
    }else if(email.length === 0){
      toast.error("Please enter email!")
    }else if(password.length === 0){
      toast.error("Please enter password!")
    }else if(confirmPassword.length === 0){
      toast.error("Please confirm password!")
    }else if(password !== confirmPassword){
      toast.error("Passwords do not match!")
    }else if(gender.length === 0){
      toast.error("Please select gender!")
    }else{
      axios.post(config.serverURL + '/user/signup', {
        firstName,
        lastName,
        email,
        password,
        phone,
        gender,
      }).then((response) => {
        const result = response.data
        if(result.status === 'error'){
          toast.error('Something went wrong :(')
          console.log(result.error)
        }else{
          toast.success('Registration successful :)')
          toast.info('Please login to continue')

          navigate('/')
        }
      }).catch((error)=> {
          console.log('error')
          console.log(error)
      })
    }
  }

  return (
    <Container>
        <Row className='mt-5'>
          <Col className='p-5 m-auto rounded-lg' lg={5} md={6} sm={12}>
            <h1 className='text-secondary mt-3 mb-5 p-1 text-center rounded'>Welcome to Bookworm</h1>  
            <Form>
                <Row>
                    <Col md={6} sm={12}>
                        <Form.Group className="mb-3" controlId="formBasicFname">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control onChange={(event) => {
                              setFirstName(event.target.value)
                            }} type="text" placeholder="Enter First Name" />
                        </Form.Group>
                    </Col>
                    <Col md={6} sm={12}>
                        <Form.Group className="mb-3" controlId="formBasicLname">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control onChange={(event) => {
                              setLastName(event.target.value)
                            }} type="text" placeholder="Enter Last Name" />
                        </Form.Group>
                    </Col>
                </Row>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control onChange={(event) => {
                              setEmail(event.target.value)
                            }} type="email" placeholder="Enter email" />
                </Form.Group>
                <Row>
                    <Col md={6} sm={12}>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control onChange={(event) => {
                              setPassword(event.target.value)
                            }} type="password" placeholder="Password" />
                        </Form.Group>
                    </Col>
                    <Col md={6} sm={12}>
                        <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control onChange={(event) => {
                              setConfirmPassword(event.target.value)
                            }} type="password" placeholder="Password" />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} sm={12}>
                        <Form.Group className="mb-3" controlId="formBasicPhone">
                            <Form.Label>Phone</Form.Label>
                            <Form.Control onChange={(event) => {
                              setPhone(event.target.value)
                            }} type="text" placeholder="Phone no." />
                        </Form.Group>
                    </Col>
                    <Col md={6} sm={12}>
                        <Form.Group className="mb-3" controlId="formBasicGender">
                            <Form.Label>Gender</Form.Label>
                            <Form.Select onChange={(event) => {
                              setGender(event.target.value)
                              console.log(gender)
                            }} aria-label="Default select example">
                              <option></option>
                              <option value="male">Male</option>
                              <option value="female">Female</option>
                              <option value="other">Other</option>
                            </Form.Select>
                        </Form.Group>
                    </Col>
                </Row>
              
              <div className='d-grid'>
                <Button onClick={signup} style={{color:'white'}} variant="info">
                  Register
                </Button>
              </div>
              <Form.Text as={Link} to='/' className='text-secondary'>
                Already a member? Login here
              </Form.Text>
            </Form>
          </Col>
        </Row>
      </Container>
  )
}

export default Register