import axios from "axios";
import React, { useState } from "react";
import { Form, Row, Col, Container, Button,Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import NavbarComp from "../components/NavbarComp";
import config from "../config";
import { MdDeleteForever } from "react-icons/md";

const UpdateProfile = () => {
const navigate = useNavigate()

   const [firstName, setFirstName] = useState("")
   const [lastName, setLastName] = useState("")
   const [phoneNumber, setPhoneNumber] = useState("")
   const updateProfile=() =>{

    if (firstName.length === 0) {
      toast.error("Please enter First name!");
    }
    else if (lastName.length === 0) {
      toast.error("Please enter last name");
    }
       else if (phoneNumber.length === 0) {
      toast.error("Please enter phone number!");

    } else {
      const formData = new FormData();

      formData.append("firstName", firstName)
      formData.append("lastName", lastName)
      formData.append("phoneNumber", phoneNumber)

      axios
        .post(config.serverURL + "/user/update-profile", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            token: sessionStorage.token
          }
        })
        .then((response) => {
          const result = response.data

        if(result.status === 'success'){
                    toast.success('UPDATED successfully :)')
                    navigate('/home')

                }else{
                    toast.error('Something went wrong :( Please try again.')
                    console.log(result.error)
                }
              }).catch((error) => {
          console.log("error")
          console.log(error)
        })
    }
  }



const styles = {
  h2: {
    marginTop: "5%",
  },
  img: {
    width: "auto",
    height: "200px",
    borderRadius: "15px",
  },
  col: { marginTop: "10px", marginBottom: "10px" },
};







  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: "20px" }}>Update Account Details</h3>
        <hr></hr>
        <Row>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setFirstName(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setLastName(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setPhoneNumber(event.target.value);
                }}
              />
            </Form.Group>
          </Col>

        </Row>
        <Row>
          <Col>
            <Button
              onClick={updateProfile}
              className="mb-3"
              style={{ color: "white" }}
              variant="info"
            >
              Edit Details
            </Button>
          </Col>
          <Col></Col>
          <Col></Col>
        </Row>
        <h4>Saved Addresses</h4>

        <Row>
          <Col style={styles.col} s={12} md={6}>
            <Card style={{ borderRadius: "15px" }}>
              <Card.Body>

                <h5>A-101,Gulmohar,</h5>
                <h5>Hill Garden,Tikujini-wadi,</h5>
                <h5>Thane-400610</h5>
                <h5>Maharashtra,India</h5>

                <Button
                  //onclick
                  className="mb-3"
                  style={{ color: "white" }}
                  variant="info"
                >
                  Edit
                </Button>
                <Button
                  className="mb-3"
                  style={{
                    //width: "50%",
                    color: "white",
                    backgroundColor: "red",
                    border: "none",
                    marginLeft: "20px",
                  }}
                  variant="info"
                >
                  <MdDeleteForever style={{ fontSize: "18px" }} /> Delete
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>

        <Button
          //onclick
          className="mb-3"
          style={{ color: "white" }}
          variant="info"
        >
          Add More Address
        </Button>
      </Container>
    </>
  );
};

export default UpdateProfile;
