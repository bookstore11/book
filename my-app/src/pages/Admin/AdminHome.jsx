import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { toast } from "react-toastify";
import BookCardAdmin from "../../components/BookCardAdmin";
import BookSection from "../../components/BookSection";
import NavbarComp from "../../components/NavbarComp";
import SearchBar from "../../components/SearchBar";
import config from "../../config";

const AdminHome = () => {
  useEffect(() => {
    loadBooks();
  }, []);

  const [bookDetails, setBookDetails] = useState([]);

  const loadBooks = () => {
    console.log("inside load books");
    axios
      .get(config.serverURL + "/admin/viewAll", {
        headers: {
          Authorization: `Bearer ${sessionStorage["jwt"]}`,
        },
      })
      .then((response) => {
        const result = response.data;
        console.log(result);
        if (result.message === "success") {
          console.log(result.data);
          setBookDetails(result.data);
        } else {
          toast.error(result.error);
        }
      })
      .catch((error) => {
        console.log("error");
        console.log(error);
      });
  };

  return (
    <>
      <NavbarComp />
      <div>
        <Container>
          <SearchBar />
          <BookSection title="Fiction" bookDetails={bookDetails} />
          {/* <BookSection title="Fiction Thriller" bookDetails={bookDetails} /> */}
        </Container>
      </div>
    </>
  );
};

export default AdminHome;
