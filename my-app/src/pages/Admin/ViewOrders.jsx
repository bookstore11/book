import React, { useState } from 'react'
import { Container, Button, Row, Col } from 'react-bootstrap'
import NavbarComp from '../../components/NavbarComp'
import ViewOrdersComponent from '../../components/ViewOrdersComponent'

const ViewOrders = () => {
  const [orderStatus, setOrderStatus] = useState('DELIVERED')
  const [btnText, setBtnText] = useState('No Action')
  const showOrders = (status) => {
    console.log(status)
    setOrderStatus(status)
    if (status === 'ORDERED') {
      setBtnText('Change to In-Transit')
    } else if (status === 'INTRANSIT') {
      setBtnText('Change to Delivered')
    } else {
      setBtnText('No Action')
    }
  }
  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: '20px' }}>View Orders</h3>
        <hr></hr>
        <Row md={12}>
          <Col md={3}>
            <Button
              onClick={() => {
                showOrders('DELIVERED')
              }}
              style={{ color: 'white', width: '80%' }}
              variant='success'>
              Delivered
            </Button>
          </Col>
          <Col md={3}>
            <Button
              onClick={() => {
                showOrders('ORDERED')
              }}
              style={{ color: 'white', width: '80%' }}
              variant='info'>
              Ordered
            </Button>
          </Col>
          <Col md={3}>
            <Button
              onClick={() => {
                showOrders('INTRANSIT')
              }}
              style={{ color: 'white', width: '80%' }}
              variant='warning'>
              In- Transit
            </Button>
          </Col>

          <Col md={3}>
            <Button
              onClick={() => {
                showOrders('CANCELLED')
              }}
              style={{ color: 'white', width: '80%' }}
              variant='danger'>
              Cancelled
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <ViewOrdersComponent status={orderStatus} btnText={btnText} />
        </Row>
      </Container>
    </>
  )
}

export default ViewOrders
