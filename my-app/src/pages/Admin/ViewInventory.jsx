import React, { useState } from 'react'
import { Form, Container, Button, Table, Modal } from 'react-bootstrap'
import NavbarComp from '../../components/NavbarComp'

const ViewInventory = () => {
  const inventoryList = [
    {
      id: 1,
      book: {
        id: 1,
        isbn: '1589712575625874',
        name: 'Becoming',
        author: 'Michelle Obama',
        language: 'English',
        price: 890.0,
        publisher: 'Viking Press',
        publishYear: '2018',
        format: 'Paperback',
        imagePath: 'images\\becoming.jpg',
        category: {
          id: 4,
          name: 'Biographies',
        },
        description:
          'Becoming is the memoir of former first lady of the United States Michelle Obama, published in 2018. Described by the author as a deeply personal experience, the book talks about her roots and how she found her voice, as well as her time in the White House, her public health campaign, and her role as a mother.',
        pages: 400,
      },
      quantity: 100.0,
    },
    {
      id: 2,
      book: {
        id: 2,
        isbn: '1589712575625875',
        name: 'Salt',
        author: 'Mark Kurlansky',
        language: 'English',
        price: 890.0,
        publisher: 'Penguin Books',
        publishYear: '2018',
        format: 'Paperback',
        imagePath: 'images\\salt.jpg',
        category: {
          id: 4,
          name: 'Biographies',
        },
        description:
          'Becoming is the memoir of former first lady of the United States Michelle Obama, published in 2018. Described by the author as a deeply personal experience, the book talks about her roots and how she found her voice, as well as her time in the White House, her public health campaign, and her role as a mother.',
        pages: 400,
      },
      quantity: 150.0,
    },
    {
      id: 3,
      book: {
        id: 3,
        isbn: '1589712575625876',
        name: 'It Ends With Us',
        author: 'Colleen Hoover',
        language: 'English',
        price: 890.0,
        publisher: 'Penguin Books',
        publishYear: '2018',
        format: 'Paperback',
        imagePath: 'images\\ItEndsWIthUs.jpg',
        category: {
          id: 1,
          name: 'Fiction',
        },
        description:
          'Becoming is the memoir of former first lady of the United States Michelle Obama, published in 2018. Described by the author as a deeply personal experience, the book talks about her roots and how she found her voice, as well as her time in the White House, her public health campaign, and her role as a mother.',
        pages: 400,
      },
      quantity: 50.0,
    },
    {
      id: 4,
      book: {
        id: 4,
        isbn: '1589712575625877',
        name: 'The Alchemist',
        author: 'Paulo Coelho',
        language: 'English',
        price: 890.0,
        publisher: 'Harper',
        publishYear: '2018',
        format: 'Paperback',
        imagePath: 'images\\alchemist.jpg',
        category: {
          id: 1,
          name: 'Fiction',
        },
        description:
          'Becoming is the memoir of former first lady of the United States Michelle Obama, published in 2018. Described by the author as a deeply personal experience, the book talks about her roots and how she found her voice, as well as her time in the White House, her public health campaign, and her role as a mother.',
        pages: 400,
      },
      quantity: 10.0,
    },
    {
      id: 5,
      book: {
        id: 5,
        isbn: '1589712575625878',
        name: 'Watchmen',
        author: 'Alan Moore',
        language: 'English',
        price: 890.0,
        publisher: 'Harper',
        publishYear: '2018',
        format: 'Paperback',
        imagePath: 'images\\watchmen.jpg',
        category: {
          id: 3,
          name: 'Comics',
        },
        description:
          'Becoming is the memoir of former first lady of the United States Michelle Obama, published in 2018. Described by the author as a deeply personal experience, the book talks about her roots and how she found her voice, as well as her time in the White House, her public health campaign, and her role as a mother.',
        pages: 400,
      },
      quantity: 75.0,
    },
  ]
  const [show, setShow] = useState(false)
  const [inventoryId, setInventoryId] = useState(false)
  const [quantity, setQuantity] = useState(false)
  const handleClose = () => setShow(false)

  const updateQuantity = () => {
    console.log(inventoryId)
    console.log(quantity)
    setShow(false)
  }
  const showModal = (inId) => {
    setInventoryId(inId)
    setShow(true)
  }
  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: '20px' }}>View Inventory</h3>
        <hr></hr>
        <Table striped>
          <thead>
            <tr>
              <th>#ID</th>
              <th>ISBN</th>
              <th>Name</th>
              <th>Author</th>
              <th>Language</th>
              <th>Price</th>
              <th>Publisher</th>
              <th>Publish Year</th>
              <th>Format</th>
              <th>Category</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
            {inventoryList.map((inventory) => {
              return (
                <tr>
                  <td>{inventory.id}</td>
                  <td>{inventory.book.isbn}</td>
                  <td>{inventory.book.name}</td>
                  <td>{inventory.book.author}</td>
                  <td>{inventory.book.language}</td>
                  <td>{inventory.book.price}</td>
                  <td>{inventory.book.publisher}</td>
                  <td>{inventory.book.publishYear}</td>
                  <td>{inventory.book.format}</td>
                  <td>{inventory.book.category.name}</td>
                  <td>{inventory.quantity}</td>
                  <td>
                    <Button
                      onClick={() => {
                        showModal(inventory.id)
                      }}
                      style={{ color: 'white' }}
                      variant='info'>
                      Edit Quantity
                    </Button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </Table>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Edit Quantity</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group
                className='mb-3'
                controlId='exampleForm.ControlInput1'>
                <Form.Label>Enter Quantity</Form.Label>
                <Form.Control
                  type='number'
                  //placeholder='name@example.com'
                  autoFocus
                  onChange={(event) => {
                    setQuantity(event.target.value)
                  }}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant='secondary' onClick={handleClose}>
              Close
            </Button>
            <Button variant='primary' onClick={updateQuantity}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
      </Container>
    </>
  )
}

export default ViewInventory
