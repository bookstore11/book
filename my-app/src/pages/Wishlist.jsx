import React from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import NavbarComp from "../components/NavbarComp";
import { MdDeleteForever } from "react-icons/md";
const Wishlist = () => {
  const styles = {
    h2: {
      marginTop: "5%",
    },
    img: {
      width: "auto",
      height: "200px",
      borderRadius: "15px",
    },
    col: { marginTop: "10px", marginBottom: "10px" },
  };

  return (
    <div>
      <NavbarComp />
      <Container>
        <h2 style={styles.h2}>Wishlist</h2>
        <hr />
        <Row>
          <Col style={styles.col} s={12} md={6}>
            <Card style={{ borderRadius: "15px" }}>
              <Card.Body>
                <Row>
                  <Col style={{ textAlign: "center" }} md={4}>
                    <img
                      src="https://m.media-amazon.com/images/I/51Z0nLAfLmL.jpg"
                      style={styles.img}
                      alt="book-cover"
                    />
                  </Col>
                  <Col md={8}>
                    <Card.Title>
                      <h4>The Alchemist - by Paulo Coelho</h4>
                    </Card.Title>
                    <Card.Text>
                      <h5>
                        <b>₹201 </b>(Inclusive of all taxes)
                      </h5>
                      <h5 style={{ color: "green" }}>In stock.</h5>
                    </Card.Text>
                    <Button
                      className="mb-1"
                      style={{
                        width: "100%",
                        color: "white",
                        backgroundColor: "red",
                        border: "none",
                      }}
                      variant="info">
                      <MdDeleteForever style={{ fontSize: "18px" }} /> Delete
                    </Button>
                    <Button
                      className="mb-1"
                      style={{ width: "100%", color: "white" }}
                      variant="info">
                      Add to Cart
                    </Button>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Wishlist;
