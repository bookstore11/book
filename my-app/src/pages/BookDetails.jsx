import { Rating } from "@mui/material";
import React from "react";
import {
  Button,
  Card,
  Col,
  Container,
  ProgressBar,
  Row,
} from "react-bootstrap";
import { TbTruckDelivery } from "react-icons/tb";
import { BiDollar } from "react-icons/bi";

import { useLocation } from "react-router-dom";
import NavbarComp from "../components/NavbarComp";
import ReviewCard from "../components/ReviewCard";

const BookDetails = () => {
  // const location = useLocation()
  // const { bookId } = location.state

  return (
    <div>
      <NavbarComp />
      {/* Actual Product details row and add to cart card */}
      <Container>
        <Row>
          <Col md={4}>
            <img
              src="https://m.media-amazon.com/images/I/51Z0nLAfLmL.jpg"
              alt="book-cover"
              style={{
                width: "auto",
                height: "100%",
                padding: "10%",
                borderRadius: "60px",
              }}></img>
          </Col>
          <Col md={4}>
            <div style={{ marginTop: "10%" }}>
              <h1>The Alchemist</h1>
              <h6>- by Paulo Coelho</h6>

              <Rating
                style={{ color: "#0091E6" }}
                name="read-only"
                value={4}
                readOnly
              />
              <p> 2156 ratings</p>
              <div style={{ fontSize: "24px", marginTop: "10%" }}>
                <h3>Product Details </h3>
                {/* <p><b>ASIN : </b>8172234988</p> */}
                <p>
                  <b>Year : </b>2005
                </p>
                <p>
                  <b>Publisher : </b>Harper
                </p>
                <p>
                  <b>Language : </b>English
                </p>
                <p>
                  <b>Paperback : </b>172 Pages
                </p>
                <p>
                  <b>ISBN-10 : </b>9788172234988
                </p>
              </div>
            </div>
          </Col>
          <Col md={4}>
            <div style={{ marginTop: "10%" }}>
              <Card style={{ width: "18rem", borderRadius: "15px" }}>
                <Card.Body>
                  <Card.Title>
                    <h4>
                      <b>Buy Now : </b>₹201 (Inclusive of all taxes)
                    </h4>
                  </Card.Title>
                  <Card.Text>
                    <h5 style={{ color: "green" }}>In stock.</h5>
                  </Card.Text>
                  <Button
                    className="mb-1"
                    style={{ width: "100%", color: "white" }}
                    variant="info">
                    Add to Cart
                  </Button>
                  <Button
                    className="mb-1"
                    style={{
                      width: "100%",
                      color: "white",
                      backgroundColor: "#0091E6",
                    }}
                    variant="info">
                    Add to Wishlist
                  </Button>
                </Card.Body>
              </Card>
            </div>
          </Col>
        </Row>
        {/* Delivery features row */}
        <Row>
          <Col md={4} style={{ textAlign: "center" }}>
            <hr />
            <BiDollar style={{ fontSize: "50px", color: "#0FEBFF" }} />
            <p style={{ fontSize: "24px" }}>Free Delivery</p>
            <hr />
          </Col>
          <Col md={4} style={{ textAlign: "center" }}>
            <hr />
            <TbTruckDelivery style={{ fontSize: "50px", color: "#0FEBFF" }} />
            <p style={{ fontSize: "24px" }}>1 Day Delivery*</p>
            <hr />
          </Col>
          <Col></Col>
        </Row>
        {/* Customer review */}
        <Row>
          <Col md={8} style={{ textAlign: "center" }}>
            <Card style={{ width: "100%", borderRadius: "15px" }}>
              <Card.Body>
                <Card.Title>
                  <h2>Customer Reviews</h2>
                </Card.Title>
                <Card.Text></Card.Text>
                <Rating
                  style={{ color: "#0091E6" }}
                  name="read-only"
                  value={4}
                  readOnly
                />
                <p> 2156 ratings</p>
                <div>
                  <Row>
                    <Col xs={4} style={{ textAlign: "right" }}>
                      <h6 style={{ display: "inline" }}>5 star</h6>
                    </Col>
                    <Col md={8} style={{ textAlign: "left", width: "50%" }}>
                      <ProgressBar now={90} style={{ borderRadius: "50px" }} />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={4} style={{ textAlign: "right" }}>
                      <h6 style={{ display: "inline" }}>4 star</h6>
                    </Col>
                    <Col md={8} style={{ textAlign: "left", width: "50%" }}>
                      <ProgressBar now={30} style={{ borderRadius: "50px" }} />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={4} style={{ textAlign: "right" }}>
                      <h6 style={{ display: "inline" }}>3 star</h6>
                    </Col>
                    <Col md={8} style={{ textAlign: "left", width: "50%" }}>
                      <ProgressBar now={50} style={{ borderRadius: "50px" }} />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={4} style={{ textAlign: "right" }}>
                      <h6 style={{ display: "inline" }}>2 star</h6>
                    </Col>
                    <Col md={8} style={{ textAlign: "left", width: "50%" }}>
                      <ProgressBar now={10} style={{ borderRadius: "50px" }} />
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={4} style={{ textAlign: "right" }}>
                      <h6 style={{ display: "inline" }}>1 star</h6>
                    </Col>
                    <Col md={8} style={{ textAlign: "left", width: "50%" }}>
                      <ProgressBar now={5} style={{ borderRadius: "50px" }} />
                    </Col>
                  </Row>
                </div>
              </Card.Body>
            </Card>
          </Col>
          <Col></Col>
        </Row>
        {/* Actual review details */}
        <ReviewCard />
      </Container>
    </div>
  );
};

export default BookDetails;
