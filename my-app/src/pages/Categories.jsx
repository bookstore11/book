import axios from "axios";
import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { toast } from "react-toastify";
import NavbarComp from "../components/NavbarComp";
import config from "../config";

const Categories = () => {
  useEffect(() => {
    loadCategories();
  }, []);
  const [categories, setCategories] = useState();

  const handleClick = () => {
    console.log("category div was clicked!");
  };

  const styles = {
    div: {
      height: "100px",
      width: "100%",
      backgroundColor: "#0091E6",
      backgroundImage: `url("https://c4.wallpaperflare.com/wallpaper/662/990/531/fantasy-book-hd-wallpaper-preview.jpg")`,
      borderRadius: "15px",
      textAlign: "center",
    },
    col: {
      padding: "10px",
    },
  };

  const loadCategories = () => {
    axios.get(config.serverURL + "/home/getCategories").then((response) => {
      const result = response.data;

      if (result.message !== "success") {
        toast.error("Something went wrong :(");
      } else {
        setCategories(result.data);
      }
    });
  };

  return (
    <div>
      <NavbarComp />
      <div style={{ margin: "25px" }}>
        <Container>
          <Row>
            {categories.map((category) => {
              return (
                <Col style={styles.col} sm={12} md={4}>
                  <div style={styles.div} onClick={handleClick}>
                    <h3
                      style={{
                        lineHeight: "100px",
                        color: "white",
                      }}>
                      {category.name}
                    </h3>
                  </div>
                </Col>
              );
            })}
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Categories;
