import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import NavbarComp from '../components/NavbarComp'
import OrderhistoryCard from '../components/OrderHistoryCard'
const ViewOrderHistory = () => {
  const listings = [
    {
      id: 1001,
      status: 'ORDERED',
      codStatus: 'true',
      statusUpdatedAt: '2022-05-06',
    },
    {
      id: 1002,
      status: 'DELIVERED',
      codStatus: 'false',
      statusUpdatedAt: '2022-05-06',
    },
    {
      id: 1003,
      status: 'CANCELLED',
      codStatus: 'true',
      statusUpdatedAt: '2022-05-06',
    },
    {
      id: 1004,
      status: 'INTRANSIT',
      codStatus: 'true',
      statusUpdatedAt: '2022-05-06',
    },
  ]

  const styles = {
    h2: {
      marginTop: '5%',
    },
    col: { marginTop: '10px', marginBottom: '10px' },
  }

  return (
    <div>
      <NavbarComp />
      <Container>
        <h2 style={styles.h2}>Order History</h2>
        <hr />
        <Row style={{ marginTop: '10px', marginBottom: '10px' }}>
          {listings.map((order) => {
            return (
              <Col style={{ marginTop: '10px', marginBottom: '10px' }} md={6}>
                <OrderhistoryCard
                  id={order.id}
                  status={order.status}
                  codStatus={order.codStatus}
                  statusUpdatedAt={order.statusUpdatedAt}></OrderhistoryCard>
              </Col>
            )
          })}
        </Row>
      </Container>
    </div>
  )
}

export default ViewOrderHistory
