import axios from "axios";
import React, { useState } from "react";
import { Form, Row, Col, Container, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import NavbarComp from "../components/NavbarComp";
import config from "../config";

const AddAddress = () => {
  const navigate = useNavigate();

  const [addressLine1, setaddressLine1] = useState("");
  const [addressLine2, setaddressLine2] = useState("");
  const [city, setCity] = useState("");
  const [state, setState] = useState("");
  const [country, setCountry] = useState("");
  const [pincode, setPincode] = useState("");


  const addAddress = () => {

    if (addressLine1.length === 0) {
      toast.error("Please enter addressLine1!");
    } else if (addressLine2.length === 0) {
      toast.error("Please enter addressLine2!");
    } else if (city.length === 0) {
      toast.error("Please enter city!");
    } else if (state.length === 0) {
      toast.error("Please enter state");
    } else if (country.length === 0) {
      toast.error("Please enter country");
    } else if (pincode.length === 0) {
    toast.error("Please enter pincode");
   } else
   {
      const formData = new FormData();

      formData.append("addressLine1", addressLine1);
      formData.append("addressLine2", addressLine2);
      formData.append("city", city);
      formData.append("state", state);
      formData.append("country", country);
      formData.append("pincode", pincode);



      axios
        .post(config.serverURL + "/user/add-address", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            token: sessionStorage.token,
          },
        })
        .then((response) => {
          const result = response.data;

          if (result.status === "success") {
            toast.success("Address added successfully :)");
            navigate("/home");
          } else {
            toast.error("Something went wrong :( Please try again.");
            console.log(result.error);
          }
        })
        .catch((error) => {
          console.log("error");
          console.log(error);
        });
    }
  };

  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: "20px" }}>Add New Address</h3>
        <hr></hr>
        <Row>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>AddressLine1</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setaddressLine1(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>AddressLine2</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setaddressLine2(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>City</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setCity(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Country</Form.Label>

              <Form.Select
                onChange={(event) => {
                  setCountry(event.target.value);
                }}
                aria-label="Default select example"
              >
                <option></option>
                <option value="India">India</option>
                <option value="Canada">Canada</option>
                <option value="England">England</option>
                <option value="Germany">Germany</option>
                <option value="Australia">Australia</option>

              </Form.Select>
            </Form.Group>
          </Col>

          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicGender">
              <Form.Label>State</Form.Label>
              <Form.Select
                onChange={(event) => {
                  setState(event.target.value);
                }}
                aria-label="Default select example"
              >
                <option></option>
                <option value="Maharashtra">Maharashtra</option>
                <option value="Tamilnadu">Tamilnadu</option>
              </Form.Select>
            </Form.Group>
          </Col>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Pincode</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setPincode(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button
              onClick={addAddress}
              className="mb-3"
              style={{ color: "white" }}
              variant="info"
            >
              Add Address
            </Button>
          </Col>
          <Col></Col>
          <Col></Col>
        </Row>
      </Container>
    </>
  );
};

export default AddAddress;
