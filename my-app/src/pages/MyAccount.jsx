import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import NavbarComp from "../components/NavbarComp";

const MyAccount = () => {
  const handleClick = () => {
    console.log("Account div was clicked!");
  };

  const styles = {
    div: {
      height: "100px",
      width: "100%",
      backgroundColor: "#0091E6",
      backgroundImage: `url("https://images2.alphacoders.com/261/26102.jpg")`,
      borderRadius: "15px",
      textAlign: "center",
    },
    div1: {
      height: "100px",
      width: "100%",
      backgroundColor: "red",
      borderRadius: "15px",
      textAlign: "center",
    },
    col: {
      padding: "10px",
    },
  };

  return (
    <div>
      <NavbarComp />
      <div style={{ margin: "25px" }}>
        <Container>
          <Row>
            <Col style={styles.col} sm={12} md={6}>
              <div style={styles.div} onClick={handleClick}>
                <h4
                  style={{
                    lineHeight: "100px",
                    color: "white",
                  }}>
                  My Account
                </h4>
              </div>
            </Col>
            <Col style={styles.col} sm={12} md={6}>
              <div style={styles.div} onClick={handleClick}>
                <h4
                  style={{
                    lineHeight: "100px",
                    color: "white",
                  }}>
                  My Orders
                </h4>
              </div>
            </Col>
            <Col style={styles.col} sm={12} md={6}>
              <div style={styles.div} onClick={handleClick}>
                <h4
                  style={{
                    lineHeight: "100px",
                    color: "white",
                  }}>
                  Wishlist
                </h4>
              </div>
            </Col>
            <Col style={styles.col} sm={12} md={6}>
              <div style={styles.div1} onClick={handleClick}>
                <h4
                  style={{
                    lineHeight: "100px",
                    color: "white",
                  }}>
                  Log Out
                </h4>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default MyAccount;
