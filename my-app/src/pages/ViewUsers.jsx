import React, { useState, useEffect } from "react";
import { Form, Container, Button, Table } from "react-bootstrap";
import { MdDeleteForever } from "react-icons/md";
import NavbarComp from "../components/NavbarComp";
import axios from "axios"

const ViewUsers = () => {
  const userList = [
    {
      id: 1,
      customer: {
        id: 1,
        firstName:"Srushti",
        lastName:"Bandal",
        email: "srushti@gmail.com",
        phoneNumber: "9876543210",
      },
    },

    {
      id: 2,
      customer: {
        id: 2,

        firstName: "Aishwarya",
        lastName: "Bhagwat",
        email: "aishwarya@gmail.com",
        phoneNumber: "9004111854",
      },
    },
    {
      id: 3,
      customer: {
        id: 3,

        firstName: "Animesh",
        lastName: "Bhagwat",
        email: "animesh@gmail.com",
        phoneNumber: "9876543211",
      },
    },
    {
      id: 4,
      customer: {
        id: 4,

        firstName: "Atharva",
        lastName: "Arote",
        email: "atharva@gmail.com",
        phoneNumber: "9920220507",
      },
    },

    {
      id: 5,
      customer: {
        id: 5,

        firstName: "Aishwarya",
        lastName: "Bhagwat",
        email: "aishwarya@gmail.com",
        phoneNumber: "2789065432",
      },
    },

    {
      id: 6,
      customer: {
        id: 6,

        firstName: "Srushti",
        lastName: "Bandal",
        email: "srushti@gmail.com",
        phoneNumber: "034567892",
      },
    },
    {
      id: 7,
      customer: {
        id: 7,

        firstName: "Animesh",
        lastName: "Bhagwat",
        email: "animesh@gmai.com",
        phoneNumber: "9044556677",
      },
    },
  ];
  // const [user, setUser] = useState([])
  // useEffect(()=>{
  //         const usefetch=async()=>{
  //             const data = await axios.get("http://localhost:7070/admin/viewUsers");
  //           console.log("hello......");
  //             console.log(data)
  //             setUser(data)

  //         }
  //         usefetch()
  // },[])


  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: "20px" }}>Customer List</h3>
        <hr></hr>
        <Table striped>
          <thead>
            <tr>
              <th>#ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
              <th>Phone Number</th>
            </tr>
          </thead>
          <tbody>
            {userList.map((user) => {
              return (
                <tr>
                  <td>{user.id}</td>
                  <td>{user.customer.firstName}</td>
                  <td>{user.customer.lastName}</td>
                  <td>{user.customer.email}</td>
                  <td>{user.customer.phoneNumber}</td>

                  <td>
                    <Button
                      className="mb-3"
                      style={{
                        //width: "50%",
                        color: "white",
                        backgroundColor: "red",
                        border: "none",
                        marginLeft: "20px",
                      }}
                      variant="info"
                    >
                      <MdDeleteForever style={{ fontSize: "18px" }} /> Delete
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Container>
    </>
  );
};

export default ViewUsers;
