
import axios from "axios";
import React, { useState } from "react";
import { Form, Row, Col, Container, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import NavbarComp from "../components/NavbarComp";
import config from "../config";

const AddDiscounts = () => {
  const navigate = useNavigate();

  const [couponCode, setcouponCode] = useState("");
  const [discountPercentage, setdiscountPercentage] = useState("");
  const [validDate, setvalidDate] = useState("");
  const [createdAt, setcreatedAt] = useState("");
  const [status, setstatus] = useState("");

  const addDiscounts = () => {
    if (couponCode.length === 0) {
      toast.error("Please enter couponCode!");
    } else if (discountPercentage.length === 0) {
      toast.error("Please enter discountPercentage!");
    } else if (validDate.length === 0) {
      toast.error("Please enter validDate!");
    } else if (createdAt.length === 0) {
      toast.error("Please enter createdAt");
    } else if (status.length === 0) {
      toast.error("Please enter status");
    } else {
      const formData = new FormData();

      formData.append("couponCode", couponCode);
      formData.append("discountPercentage", discountPercentage);
      formData.append("validDate", validDate);
      formData.append("createdAt", createdAt);
      formData.append("status", status);

      axios
        .post(config.serverURL + "/admin/add-discount", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
            token: sessionStorage.token,
          },
        })
        .then((response) => {
          const result = response.data;

          if (result.status === "success") {
            toast.success("discount added successfully :)");
            navigate("/home");
          } else {
            toast.error("Something went wrong :( Please try again.");
            console.log(result.error);
          }
        })
        .catch((error) => {
          console.log("error");
          console.log(error);
        });
    }
  };

  return (
    <>
      <NavbarComp />
      <Container>
        <h3 style={{ marginTop: "40px" }}>Manage Discounts</h3>
        <hr></hr>
        <Row>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Coupon Code</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setcouponCode(event.target.value);
                }}
              />
            </Form.Group>
          </Col>

          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Discount Percentage</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setdiscountPercentage(event.target.value);
                }}
              />
            </Form.Group>
          </Col>

          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Valid Date</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setvalidDate(event.target.value);
                }}
              />
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Created At</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setcreatedAt(event.target.value);
                }}
              />
            </Form.Group>
          </Col>

          <Col md={4}>
            <Form.Group className="mb-3" controlId="formBasicFname">
              <Form.Label>Status</Form.Label>
              <Form.Control
                type="text"
                onChange={(event) => {
                  setstatus(event.target.value);
                }}
              />
            </Form.Group>
          </Col>

          <Col>
            <Button
              onClick={addDiscounts}
              className="mb-3"
              style={{ color: "white", marginTop: "30px" }}
              variant="info"
            >
              Add New Coupon
            </Button>
          </Col>

          <Col></Col>
        </Row>

      </Container>
    </>
  );
};

export default AddDiscounts;

