import React from "react";
import { Button, Card, Col, Container, Form, Row } from "react-bootstrap";
import NavbarComp from "../components/NavbarComp";

const Checkout = () => {
  const styles = {
    h2: {
      marginTop: "5%",
    },
    img: {
      width: "auto",
      height: "200px",
      borderRadius: "15px",
    },
    col: { marginTop: "10px", marginBottom: "10px" },
  };

  return (
    <div>
      <NavbarComp />
      <Container>
        <Row>
          <Col sm={12} md={8}>
            <h2 style={styles.h2}>Select Delivery Address</h2>
            <hr />
            <div style={{ marginTop: "20px", marginBottom: "20px" }}>
              <Card style={{ borderRadius: "15px" }}>
                <Card.Body>
                  <Row>
                    <Col sm={1}>
                      <Form.Check
                        value="addressId"
                        type="radio"
                        name="address"
                        aria-label="radio 1"
                      />
                    </Col>
                    <Col sm={11}>
                      <Card.Title></Card.Title>
                      <Card.Text>
                        <h5>Address Line 1</h5>
                        <h5>Address Line 2</h5>
                        <h5>City - Pincode</h5>
                        <h5>State, Country</h5>
                        <h5>Phone Number</h5>
                      </Card.Text>
                    </Col>
                  </Row>
                </Card.Body>
              </Card>
            </div>
          </Col>
          <Col sm={12} md={4}>
            <div style={{ marginTop: "10%" }}>
              <h2 style={styles.h2}>Payment Method</h2>
              <hr />
              <Form.Check
                type="radio"
                name="payment"
                id={`default-radio`}
                label={`Online Payment`}
              />
              <Form.Check
                type="radio"
                name="payment"
                id={`default-radio`}
                label={`Cash On Delivery`}
              />
            </div>
          </Col>
        </Row>
        <Row>
          <Col>
            <Button
              className="mb-1"
              style={{ width: "100%", color: "white" }}
              variant="info">
              Proceed To Payment
            </Button>
          </Col>
          <Col></Col>
          <Col></Col>
        </Row>
      </Container>
    </div>
  );
};

export default Checkout;
