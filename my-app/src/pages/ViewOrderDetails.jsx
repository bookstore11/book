import React from 'react'
import { Button, Card, Col, Container, ProgressBar, Row } from 'react-bootstrap'
import NavbarComp from '../components/NavbarComp'

const ViewOrderDetails = () => {
  let cancelBtnStatus = true
  let reviewBtnStatus = false
  let reviewBtnVariant = 'secondary'
  let cancelBtnVariant = 'secondary'
  const styles = {
    h2: {
      marginTop: '5%',
    },
    col: { marginTop: '10px', marginBottom: '10px' },
  }

  const booklist = [
    {
      book: {
        name: 'Salt',
        author: 'Mark Kurlansky',
        price: 890.0,
        imagePath: 'images\\salt.jpg',
      },
      quantity: 5,
    },
    {
      book: {
        name: 'Becoming',
        author: 'Michelle Obama',
        price: 890.0,
        imagePath: 'images\\salt.jpg',
      },
      quantity: 2,
    },
    {
      book: {
        name: 'Salt',
        author: 'Mark Kurlansky',
        price: 890.0,
        imagePath: 'images\\salt.jpg',
      },
      quantity: 5,
    },
  ]

  const createdAt = '2022-05-05'
  const id = 2
  const orderTotal = 550
  const codStatus = false
  const paymentText = codStatus ? 'Cash On Delivery' : 'Paid Online'
  const status = 'ORDERED'
  let now, variant, label
  switch (status) {
    case 'ORDERED':
      now = 25
      variant = 'info'
      label = status
      cancelBtnVariant = 'danger'
      break

    case 'INTRANSIT':
      now = 50
      variant = 'warning'
      cancelBtnVariant = 'danger'
      label = status
      break

    case 'DELIVERED':
      now = 75
      variant = 'success'
      label = status
      cancelBtnStatus = false
      reviewBtnStatus = true
      reviewBtnVariant = 'info'
      break

    case 'CANCELLED':
      now = 100
      variant = 'danger'
      label = status
      cancelBtnStatus = false
      break
    default:
      break
  }

  return (
    <div>
      <NavbarComp />
      <Container>
        <h2 style={styles.h2}>Order Details</h2>
        <hr />

        <h5>Order Details</h5>
        <Card style={{ width: '100%', borderRadius: '15px' }}>
          <Card.Body>
            Order Date : {createdAt} <br />
            Order # : {id} <br />
            Order Total : {orderTotal} Rs <br />
          </Card.Body>
        </Card>

        <hr />
        <h5>Shipment Details</h5>
        <Card style={{ width: '100%', borderRadius: '15px' }}>
          <Card.Body>
            <Row>
              <ProgressBar>
                <ProgressBar variant={variant} now={now} label={label} />
              </ProgressBar>
            </Row>
            <br />
            {booklist.map((BookDetails) => {
              return (
                <Row md={10}>
                  <Col md={4}>
                    <img
                      src='https://m.media-amazon.com/images/I/51Z0nLAfLmL.jpg'
                      alt='book-cover'
                      style={{
                        width: '250px',
                        height: '320px',
                        padding: '10%',
                        borderRadius: '60px',
                      }}></img>
                  </Col>
                  <Col md={4}>
                    <br />
                    <br />
                    <Row>
                      <h4>{BookDetails.book.name}</h4>
                    </Row>
                    <Row>
                      <h5>- by {BookDetails.book.author}</h5>
                    </Row>
                    <br />
                    <Row>
                      <h5>
                        <b>{BookDetails.book.price}</b> Rs
                      </h5>
                    </Row>
                    <br />

                    <Row>
                      <h5>Qty : {BookDetails.quantity}</h5>
                    </Row>
                    <Row>
                      <Button
                        disabled={!reviewBtnStatus}
                        className='mb-1'
                        style={{ width: '80%', color: 'white', margin: '10px' }}
                        variant={reviewBtnVariant}>
                        Add Book Review
                      </Button>
                    </Row>
                  </Col>
                </Row>
              )
            })}
          </Card.Body>
        </Card>
        <hr />
        <h5>Payment Information</h5>
        <Card style={{ width: '100%', borderRadius: '15px' }}>
          <Card.Body>
            Paid via : {paymentText} <br />
          </Card.Body>
        </Card>
        <hr />
        <h5>Shipping Address</h5>
        <Card style={{ width: '100%', borderRadius: '15px' }}>
          <Card.Body>
            Address line 1 <br />
            Address line 2 <br />
            city <br />
            state <br />
            411028 <br />
            Phone : 9405358712
            <br />
          </Card.Body>
        </Card>

        <br />
        <Row md={12}>
          <Col md={5}>
            <Button
              className='mb-1'
              style={{ width: '100%', color: 'white', margin: '10px' }}
              variant='info'>
              Back
            </Button>
          </Col>
          <Col md={5}>
            <Button
              disabled={!cancelBtnStatus}
              className='mb-1'
              style={{ width: '100%', color: 'white', margin: '10px' }}
              variant={cancelBtnVariant}>
              Cancel Order
            </Button>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default ViewOrderDetails
