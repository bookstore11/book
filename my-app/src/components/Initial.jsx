import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from "../pages/Register";
import Home from "../pages/Home";
import Login from "../pages/Login";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AddBook from "../pages/AddBook";
import BookDetails from "../pages/BookDetails";
import Wishlist from "../pages/Wishlist";
import ShoppingCart from "../pages/ShoppingCart";
import Checkout from "../pages/Checkout";
import ViewOrderHistory from "../pages/ViewOrderHistory";
import ViewOrderDetails from "../pages/ViewOrderDetails";
import EditBook from "../pages/Admin/EditBook";
import ViewInventory from "../pages/Admin/ViewInventory";
import ViewOrders from "../pages/Admin/ViewOrders";
import AddAddress from "../pages/AddAddress";
import UpdateProfile from "./../pages/UpdateProfile";
import AddDiscounts from "./../pages/AddDiscounts";
import ViewDiscounts from "./../pages/ViewDiscounts";
import ViewUsers from "./../pages/ViewUsers";
import Categories from "../pages/Categories";
import MyAccount from "../pages/MyAccount";
import AdminMenu from "../pages/AdminMenu";
import AdminHome from "../pages/Admin/AdminHome";

const Initial = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        {/* <Route path="/bookDetails" element={<BookDetails/>} /> */}
        <Route path="/home" element={<Home />} />
        <Route path="/admin-home" element={<AdminHome />} />
        <Route path="/register" element={<Register />} />
        <Route path="/add-book" element={<AddBook />} />
        <Route path="/book-details" element={<BookDetails />} />
        <Route path="/wishlist" element={<Wishlist />} />
        <Route path="/update-profile" element={<UpdateProfile />} />
        <Route path="/cart" element={<ShoppingCart />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/viewOrderHistory" element={<ViewOrderHistory />} />
        <Route path="/viewOrderDetails" element={<ViewOrderDetails />} />
        <Route path="/editBook" element={<EditBook />} />
        <Route path="/viewInventory" element={<ViewInventory />} />
        <Route path="/viewOrders" element={<ViewOrders />} />
        <Route path="/home" element={<Home />} />
        <Route path="/register" element={<Register />} />
        <Route path="/add-book" element={<AddBook />} />
        <Route path="/book-details" element={<BookDetails />} />
        <Route path="/wishlist" element={<Wishlist />} />
        <Route path="/cart" element={<ShoppingCart />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/addAddress" element={<AddAddress />} />
        <Route path="/updateProfile" element={<UpdateProfile />} />
        <Route path="/addDiscounts" element={<AddDiscounts />} />
        <Route path="/viewDiscounts" element={<ViewDiscounts />} />
        <Route path="/viewUsers" element={<ViewUsers />} />
        <Route path="/viewOrderHistory" element={<ViewOrderHistory />} />
        <Route path="/viewOrderDetails" element={<ViewOrderDetails />} />
        <Route path="/categories" element={<Categories />} />
        <Route path="/account" element={<MyAccount />} />
        <Route path="/admin-menu" element={<AdminMenu />} />
      </Routes>
      <ToastContainer position="top-center" autoClose={1500} />
    </BrowserRouter>
  );
};

export default Initial;
