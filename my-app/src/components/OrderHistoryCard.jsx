import React from 'react'
import { Button, Card, Col, Row } from 'react-bootstrap'

const OrderHistoryCard = (props) => {
  const { id, status, codStatus, statusUpdatedAt } = props
  const text = codStatus ? 'Cash On Delivery' : 'Paid Online'

  const styles = {
    h2: {
      marginTop: '5%',
    },
    img: {
      width: '150px',
      height: '200px',
      borderRadius: '15px',
    },
    col: { marginTop: '10px', marginBottom: '10px' },
  }

  return (
    <Card style={{ borderRadius: '15px' }}>
      <Card.Body>
        <Row>
          <Col style={{ textAlign: 'center' }} md={4}>
            <img
              src='https://miro.medium.com/max/2400/1*hcPAb0W2Ki424RtZhKKPVw.png'
              style={styles.img}
              alt='book-status'
            />
          </Col>
          <Col md={8}>
            <Card.Title>
              <h4>
                <b>{status}</b>
              </h4>
            </Card.Title>
            <Card.Text>
              <h5>on {statusUpdatedAt}</h5>
            </Card.Text>
            <Card.Text>
              <h6>Order Id: {id}</h6>
              <h6>Payment : {text}</h6>
            </Card.Text>
            <Button
              className='mb-1'
              style={{ width: '100%', color: 'white' }}
              variant='info'>
              View Order Details
            </Button>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  )
}

export default OrderHistoryCard
