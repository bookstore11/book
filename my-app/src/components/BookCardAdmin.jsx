import React from "react";
import { FavoriteOutlined } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { Button, Card, Col, Row } from "react-bootstrap";
import { useState } from "react";
import config from "../config";
import { Link } from "react-router-dom";

const BookCardAdmin = () => {
  //   const width = window.innerWidth;

  //   const { details } = props;
  //   const [clicked, setClicked] = useState(false);

  function truncatepc(sentence) {
    if (sentence.length > 17) {
      return sentence.substring(0, 14) + "...";
    } else {
      return sentence;
    }
  }

  function truncatemobile(sentence) {
    if (sentence.length > 15) {
      return sentence.substring(0, 12) + "...";
    } else {
      return sentence;
    }
  }

  //   const imageURL = config.serverURL + "/" + details.image;

  return (
    <Card
      className="m-1"
      style={{ display: "inline-block", width: "14rem", borderRadius: 10 }}>
      {/* <IconButton
        onClick={clickHandler}
        style={{ position: "absolute", left: "81%" }}>
        <FavoriteOutlined
          style={clicked ? { color: "red" } : { color: "#AAA", opacity: "70%" }}
        />
      </IconButton> */}
      <Card.Img
        className="p-3"
        style={{ height: "18rem", borderRadius: "1.5rem" }}
        variant="top"
        src="https://m.media-amazon.com/images/I/51Z0nLAfLmL.jpg"
      />
      <Card.Body>
        <Link
          style={{ color: "black", textDecoration: "none" }}
          to="/book-details">
          <Card.Title style={{ fontWeight: "bold" }}>The Alchemist</Card.Title>
          <Card.Subtitle className="mb-2">- by Paulo Coelho</Card.Subtitle>
        </Link>
        <Card.Text style={{ color: "#b12704", fontWeight: "bold" }}>
          &#8377;201
          <span style={{ fontSize: "12px" }}> (inclusive of all taxes)</span>
        </Card.Text>
        <Row>
          <Col sm={12} md={6}>
            <Button
              className="mb-1"
              style={{ width: "100%", color: "white" }}
              variant="info">
              Edit
            </Button>
          </Col>
          <Col sm={12} md={6}>
            <Button className="mb-1" style={{ width: "100%" }} variant="danger">
              Delete
            </Button>
          </Col>
        </Row>
        {/* <a style={{position: 'absolute',top: '0', left: '0',height: '100%', width: '100%'}} href='/book-details'></a> */}
      </Card.Body>
    </Card>
  );

  //   if (width >= 768) {
  //     return (
  //       <Card
  //         className="m-1"
  //         style={{ display: "inline-block", width: "14rem", borderRadius: 10 }}>
  //         <IconButton
  //           onClick={clickHandler}
  //           style={{ position: "absolute", left: "81%" }}>
  //           <FavoriteOutlined
  //             style={
  //               clicked ? { color: "red" } : { color: "#AAA", opacity: "70%" }
  //             }
  //           />
  //         </IconButton>
  //         <Card.Img
  //           className="p-3"
  //           style={{ height: "18rem", borderRadius: "1.5rem" }}
  //           variant="top"
  //           src={imageURL}
  //         />
  //         <Card.Body>
  //           <Link
  //             style={{ color: "black", textDecoration: "none" }}
  //             to="/book-details"
  //             state={{ bookId: details.id }}>
  //             <Card.Title style={{ fontWeight: "bold" }}>
  //               {truncatepc(details.name)}
  //             </Card.Title>
  //             <Card.Subtitle className="mb-2">
  //               - by {details.author}
  //             </Card.Subtitle>
  //           </Link>
  //           <Card.Text style={{ color: "#b12704", fontWeight: "bold" }}>
  //             &#8377;{details.price}
  //             <span style={{ fontSize: "12px" }}> (inclusive of all taxes)</span>
  //           </Card.Text>
  //           <Button
  //             className="mb-1"
  //             style={{ width: "100%", color: "white" }}
  //             variant="info">
  //             Add to Cart
  //           </Button>
  //           {/* <Button className='mt-1' style={{width:'100%'}} variant="outline-primary">Add to wishlist</Button> */}
  //           {/* <a style={{position: 'absolute',top: '0', left: '0',height: '100%', width: '100%'}} href='/book-details'></a> */}
  //         </Card.Body>
  //       </Card>
  //     );
  //   } else {
  //     return (
  //       <Card
  //         className="m-1"
  //         style={{ display: "inline-block", width: "11rem", borderRadius: 10 }}>
  //         <IconButton
  //           onClick={clickHandler}
  //           style={{ position: "absolute", left: "81%" }}>
  //           <FavoriteOutlined
  //             style={
  //               clicked ? { color: "red" } : { color: "#AAA", opacity: "70%" }
  //             }
  //           />
  //         </IconButton>
  //         <Card.Img
  //           className="p-3"
  //           style={{ height: "14rem", borderRadius: "1.5rem" }}
  //           variant="top"
  //           src={imageURL}
  //         />
  //         <Card.Body>
  //           <Card.Title style={{ fontWeight: "bold" }}>
  //             {truncatemobile(details.name)}
  //           </Card.Title>
  //           <Card.Subtitle className="mb-2">- by {details.author}</Card.Subtitle>
  //           <Card.Text style={{ color: "#b12704", fontWeight: "bold" }}>
  //             &#8377;{details.price}
  //             <span style={{ fontSize: "12px" }}> (inclusive of all taxes)</span>
  //           </Card.Text>
  //           <Button
  //             className="mb-1"
  //             style={{ width: "100%", color: "white" }}
  //             variant="info">
  //             Edit
  //           </Button>
  //           <Button
  //             className="mt-1"
  //             style={{ width: "100%" }}
  //             variant="outline-danger">
  //             Delete
  //           </Button>
  //         </Card.Body>
  //       </Card>
  //     );
  //   }
};

export default BookCardAdmin;
