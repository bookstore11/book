import React from "react";
import BookCard from "./BookCard";

const BookSection = (props) => {
  const { title, bookDetails } = props;
  console.log("Inside book section");
  console.log(bookDetails);

  return (
    <div>
      <h2 className="mt-3" style={{ fontFamily: "Varela Round, sans-serif" }}>
        {title}
      </h2>
      <div>
        {bookDetails.map((book) => {
          return <BookCard key={book.id} details={book} />;
        })}
      </div>
    </div>
  );
};

export default BookSection;
