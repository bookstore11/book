import React from 'react'
import { Button, Form, FormControl } from 'react-bootstrap'

const SearchBar = () => {
  return (
    <Form className="d-flex mt-2">
        <FormControl
        type="search"
        placeholder="Search for your book here..."
        className="me-2"
        aria-label="Search"
        />
        {/* <Dropdown className='me-2'>
            <Dropdown.Toggle variant="outline-secondary" id="dropdown-basic">
                Filters
            </Dropdown.Toggle>

            <Dropdown.Menu variant='dark'>
              <Dropdown.Header>Filters</Dropdown.Header>
              <Dropdown.Item >Title</Dropdown.Item>
              <Dropdown.Item >Author</Dropdown.Item>
              <Dropdown.Item >ISBN</Dropdown.Item>
              <Dropdown.Item >Genre</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown> */}
        <Form.Select style={{marginRight: '8px', width:'30%', color:'grey', borderColor:'#ced4da'}} aria-label="Default select example">
        <option>Filter</option>
          <option value="1">Title</option>
          <option value="2">Author</option>
          <option value="3">ISBN</option>
          <option value="4">Genre</option>
        </Form.Select>
        <Button style={{color: 'white'}} variant="info"><i className="fa-solid fa-magnifying-glass"></i></Button>
    </Form>
  );
}

export default SearchBar