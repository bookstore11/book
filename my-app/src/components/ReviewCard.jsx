import React from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import ReviewInfo from './ReviewInfo'

const ReviewCard = () => {
  return (
    <div style={{marginTop:'5%'}}>
        <Row>
            <Col md={8}>
            <Card style={{ width: '100%', borderRadius:'15px' }}>
                <Card.Body>
                    <div style={{padding:'3%'}}>
                        <Card.Title><h2 style={{marginBottom:'5%'}}>All Reviews</h2></Card.Title>
                        <div style={{overflow:'auto', height:'325px'}}>
                            <ReviewInfo />
                            <ReviewInfo />
                        </div>
                    </div>
                </Card.Body>
            </Card>
            </Col>
            <Col></Col>
        </Row>
    </div>
  )
}

export default ReviewCard