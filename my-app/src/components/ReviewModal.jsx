import { Rating } from "@mui/material";
import React, { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";

const ReviewModal = () => {
  const [show, setShow] = useState(false);
  const [value, setValue] = useState(0);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <div>
      <Button style={{ color: "white" }} variant="info" onClick={handleShow}>
        Review This Book
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add book review</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Give Overall Rating</h5>
          <Rating
            style={{ color: "#0091E6" }}
            name="simple-controlled"
            value={value}
            onChange={(event, newValue) => {
              console.log(newValue);
              setValue(newValue);
            }}
          />
          <Form.Group className="mb-3" controlId="formBasicFname">
            <Form.Label>Title*</Form.Label>
            <Form.Control type="text" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Description*</Form.Label>
            <Form.Control
              style={{ resize: "none" }}
              as="textarea"
              maxLength={1000}
              rows={5}
            />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            style={{ color: "white" }}
            variant="info"
            onClick={handleClose}>
            Add Review
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ReviewModal;
