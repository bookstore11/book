import { Link } from "react-router-dom";
import { Navbar, Nav, Container } from "react-bootstrap";
import IconButton from "@mui/material/IconButton";
import { ShoppingCart } from "@mui/icons-material";
import { Badge } from "@mui/material";

const NavbarComp = () => {
  const userRole = true;

  return (
    <div>
      <Navbar
        style={{ textAlign: "center" }}
        collapseOnSelect
        bg="light"
        expand="lg"
        variant="light">
        <Container>
          <Navbar.Brand as={Link} to="/home">
            BookWorm
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/home">
                Home
              </Nav.Link>
              <Nav.Link as={Link} to="/categories">
                Categories
              </Nav.Link>
              <Nav.Link as={Link} to="/account">
                Account
              </Nav.Link>
            </Nav>
            {userRole ? (
              <Nav className="justify-content-right">
                <Nav.Item>
                  <Nav.Link as={Link} to="/cart">
                    <Badge badgeContent={4} color="info">
                      <IconButton>
                        <ShoppingCart sx={{ color: "#AAA" }} />
                      </IconButton>
                    </Badge>
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            ) : (
              <></>
            )}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavbarComp;
