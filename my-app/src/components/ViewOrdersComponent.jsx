import { Button, Table } from 'react-bootstrap'

const ViewOrdersComponent = (props) => {
  const orderList = [
    {
      id: 2,
      user: {
        id: 5,
        firstName: 'Niya',
        lastName: 'Bisht',
        email: 'niya@gmail.com',
        password:
          '$2a$10$PA5x6YfPEsZhTq7DB5xKG.THpCDnrGkwQr7SWYv4kFMWvKkjCMjXS',
        phone: '9403179466',
        gender: 'FEMALE',
        joinedOn: '2022-09-13T12:55:49.000+00:00',
        userRoles: [
          {
            id: 2,
            roleName: 'USER',
          },
        ],
      },
      createdAt: '2022-09-13',
      status: 'DELIVERED',
      address: {
        id: 5,
        addressLine1: '601, Bakul, Kubera Sankul',
        addressLine2: 'Near Sanjeevani Hospital, Hadapsar',
        city: 'Pune',
        state: 'Maharashtra',
        country: 'India',
        pincode: '411028',
        user: {
          id: 5,
          firstName: 'Niya',
          lastName: 'Bisht',
          email: 'niya@gmail.com',
          password:
            '$2a$10$PA5x6YfPEsZhTq7DB5xKG.THpCDnrGkwQr7SWYv4kFMWvKkjCMjXS',
          phone: '9403179466',
          gender: 'FEMALE',
          joinedOn: '2022-09-13T12:55:49.000+00:00',
          userRoles: [
            {
              id: 2,
              roleName: 'USER',
            },
          ],
        },
      },
      codStatus: false,
      statusUpdatedAt: '2022-09-13',
    },
    {
      id: 6,
      user: {
        id: 3,
        firstName: 'Riya',
        lastName: 'Bisht',
        email: 'riya@gmail.com',
        password:
          '$2a$10$3AznJ7pPCEDogA8vCdO66.uzx68sQHyJd5dpj8webpTIT.paxO9vK',
        phone: '9403179464',
        gender: 'FEMALE',
        joinedOn: '2022-09-13T12:55:23.000+00:00',
        userRoles: [
          {
            id: 2,
            roleName: 'USER',
          },
        ],
      },
      createdAt: '2022-09-13',
      status: 'DELIVERED',
      address: {
        id: 2,
        addressLine1: '202, Champa, Kubera Sankul',
        addressLine2: 'Near Sanjeevani Hospital, Hadapsar',
        city: 'Pune',
        state: 'Maharashtra',
        country: 'India',
        pincode: '411028',
        user: {
          id: 3,
          firstName: 'Riya',
          lastName: 'Bisht',
          email: 'riya@gmail.com',
          password:
            '$2a$10$3AznJ7pPCEDogA8vCdO66.uzx68sQHyJd5dpj8webpTIT.paxO9vK',
          phone: '9403179464',
          gender: 'FEMALE',
          joinedOn: '2022-09-13T12:55:23.000+00:00',
          userRoles: [
            {
              id: 2,
              roleName: 'USER',
            },
          ],
        },
      },
      codStatus: false,
      statusUpdatedAt: '2022-09-13',
    },
  ]
  const { status, btnText } = props
  const btnDisabled = btnText === 'No Action' ? true : false
  const btnVariant = btnText === 'No Action' ? 'secondary' : 'info'
  return (
    <div>
      <Table striped>
        <thead>
          <tr>
            <th>Order Id</th>
            <th>Order Date</th>
            <th>Status</th>
            <th>Last Updated At</th>
          </tr>
        </thead>
        <tbody>
          {orderList.map((order) => {
            return (
              <tr>
                <td>{order.id}</td>
                <td>{order.createdAt}</td>
                <td>{status}</td>
                <td>{order.statusUpdatedAt}</td>
                <td>
                  <Button
                    className='mb-1'
                    style={{ width: '100%', color: 'white' }}
                    variant={btnVariant}
                    disabled={btnDisabled}>
                    {btnText}
                  </Button>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>
    </div>
  )
}

export default ViewOrdersComponent
