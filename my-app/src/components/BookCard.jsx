import React, { useEffect } from "react";
import { FavoriteOutlined } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { Button, Card } from "react-bootstrap";
import { useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import config from "../config";

const BookCard = (props) => {
  useEffect(() => {
    getImage();

    return () => {
      URL.revokeObjectURL(objectURL);
    };
  }, []);

  const width = window.innerWidth;

  const { details } = props;
  const [clicked, setClicked] = useState(false);
  const [image, setImage] = useState();

  const getImage = (path) => {
    axios
      .get(config.serverURL + `/home/${details.id}/images`, {
        responseType: "blob",
      })
      .then((response) => {
        console.log(response.data);
        setImage(response.data);
      });
  };

  function truncatepc(sentence) {
    if (sentence.length > 17) {
      return sentence.substring(0, 14) + "...";
    } else {
      return sentence;
    }
  }

  function truncatemobile(sentence) {
    if (sentence.length > 15) {
      return sentence.substring(0, 12) + "...";
    } else {
      return sentence;
    }
  }

  function clickHandler() {
    console.log(clicked);
    clicked ? setClicked(false) : setClicked(true);
  }

  const objectURL = URL.createObjectURL(image);

  if (width >= 768) {
    return (
      <Card
        className="m-1"
        style={{ display: "inline-block", width: "14rem", borderRadius: 10 }}>
        <IconButton
          onClick={clickHandler}
          style={{ position: "absolute", left: "81%" }}>
          <FavoriteOutlined
            style={
              clicked ? { color: "red" } : { color: "#AAA", opacity: "70%" }
            }
          />
        </IconButton>
        <Card.Img
          className="p-3"
          style={{ height: "18rem", borderRadius: "1.5rem" }}
          variant="top"
          src={objectURL}
        />
        <Card.Body>
          <Link
            style={{ color: "black", textDecoration: "none" }}
            to="/book-details"
            state={{ bookId: details.id }}>
            <Card.Title style={{ fontWeight: "bold" }}>
              {truncatepc(details.name)}
            </Card.Title>
            <Card.Subtitle className="mb-2">
              - by {details.author}
            </Card.Subtitle>
          </Link>
          <Card.Text style={{ color: "#b12704", fontWeight: "bold" }}>
            &#8377;{details.price}
            <span style={{ fontSize: "12px" }}> (inclusive of all taxes)</span>
          </Card.Text>
          <Button
            className="mb-1"
            style={{ width: "100%", color: "white" }}
            variant="info">
            Add to Cart
          </Button>
          {/* <Button className='mt-1' style={{width:'100%'}} variant="outline-primary">Add to wishlist</Button> */}
          {/* <a style={{position: 'absolute',top: '0', left: '0',height: '100%', width: '100%'}} href='/book-details'></a> */}
        </Card.Body>
      </Card>
    );
  } else {
    return (
      <Card
        className="m-1"
        style={{ display: "inline-block", width: "11rem", borderRadius: 10 }}>
        <IconButton
          onClick={clickHandler}
          style={{ position: "absolute", left: "81%" }}>
          <FavoriteOutlined
            style={
              clicked ? { color: "red" } : { color: "#AAA", opacity: "70%" }
            }
          />
        </IconButton>
        <Card.Img
          className="p-3"
          style={{ height: "14rem", borderRadius: "1.5rem" }}
          variant="top"
          src={objectURL}
        />
        <Card.Body>
          <Card.Title style={{ fontWeight: "bold" }}>
            {truncatemobile(details.name)}
          </Card.Title>
          <Card.Subtitle className="mb-2">- by {details.author}</Card.Subtitle>
          <Card.Text style={{ color: "#b12704", fontWeight: "bold" }}>
            &#8377;{details.price}
            <span style={{ fontSize: "12px" }}> (inclusive of all taxes)</span>
          </Card.Text>
          <Button
            className="mb-1"
            style={{ width: "100%", color: "white" }}
            variant="info">
            Add to Cart
          </Button>
          {/* <Button className='mt-1' style={{width:'100%'}} variant="outline-primary">Add to wishlist</Button> */}
        </Card.Body>
      </Card>
    );
  }
};

export default BookCard;
