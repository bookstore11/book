import React from 'react'
import { Rating } from '@mui/material'
import { MdAccountCircle } from 'react-icons/md';
import { Col, Row } from 'react-bootstrap';

const ReviewInfo = () => {
  return (
    <div>
        <Row>
        <Col xs={1}><MdAccountCircle style={{fontSize:'50px'}} /></Col>
        <Col><h4 style={{position:'relative', top:'20%'}}>Srushti Bandal</h4></Col>
        </Row>
        <h5 style={{marginTop:'3%'}}><b>Review Title</b></h5>
        <Rating style={{color:"#0091E6"}} name="read-only" value={4} readOnly />
        <p>This is probably the most intresting book I've read so far, the writing so the book is amazing and can be easily understand. If you are first time reader then I would definitely recommend this book. It is was originally written in Portuguese and later translated in English.The story is about a boy from Andalsuia and his adventures through African desert to find treasure in Pyramids of Egypt. One can easily relate with the story because we all have our own goals and this book will teach you that no matter how many obstacles come in your way you should never give up</p>
    </div>
  )
}

export default ReviewInfo